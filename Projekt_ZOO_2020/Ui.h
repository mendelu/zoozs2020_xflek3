//
// Created by david on 20.11.2020.
//

#ifndef MAIN_CPP_UI_H
#define MAIN_CPP_UI_H
#include "Player.h"
#include "Monstra/TovarnaMonster.h"
#include "Monstra/TovarnaStrednichMonster.h"
#include "Monstra/TovarnaTezkychMonster.h"
#include "MapGenerator.h"
#include "Lokace/Lokace.h"
#include "MetodyPlayer.h"
#include "Lokace/MistaSpawn/Kaluz.h"
class Ui {
    Lokace* s_aktualniLokaceHrace;
    Mista* s_aktualniMistoHrace;
    Inventory* m_inventar;
public:
    Ui();
    void vypisMoznosti();
    void co1();
    void co2();
    void co3();
    void co4();
    void co5();
    void coTed();
    void coTu();
    void prvniZapnutiHry();
    void zvoleniObtiznosti();
    void kontrolaVypnutiHry();
    void vypnutiHry();
    void vypisZlataky(float pocetZlataku);
    void overovaniVstupuProPorozhlednutiPoOkoli();
    void vygenerovaniLokaci();
    void porozhledniSe();
    int ziskejCisloPozice();
    int pocetMistVLokaci();
    void zmenMisto(int cislo);
    bool getHotoveMisto();

};


#endif //MAIN_CPP_UI_H
