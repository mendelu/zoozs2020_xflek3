//
// Created by david on 25.12.2020.
//

#include "SpecialniPredmety.h"
SpecialniPredmety::SpecialniPredmety(std::string jmeno, std::string vzacnost, std::string kod) {
    m_jmeno = jmeno;
    m_vzacnost = vzacnost;
    m_kod = kod;
}
std::string SpecialniPredmety::getJmeno(){
    return m_jmeno;
}
std::string SpecialniPredmety::getVzacnost(){
    return m_vzacnost;
}
std::string SpecialniPredmety::getKod(){
    return m_kod;
}
std::string SpecialniPredmety::getInfo(){
    return getJmeno() + "\tVzacnost: " + getVzacnost() + "\n";
}