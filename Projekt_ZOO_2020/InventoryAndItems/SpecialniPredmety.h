//
// Created by david on 25.12.2020.
//

#ifndef PROJEKT_ZOO_2020_SPECIALNIPREDMETY_H
#define PROJEKT_ZOO_2020_SPECIALNIPREDMETY_H
#include <iostream>

class SpecialniPredmety {
    std::string m_jmeno;
    std::string m_vzacnost;
    std::string m_kod; // na ověřování že hráč má správný speciální item (třeba klíč ke dveřím)
public:
    SpecialniPredmety(std::string jmeno, std::string vzacnost, std::string kod);
    std::string getJmeno();
    std::string getVzacnost();
    std::string getKod();
    std::string getInfo();
};


#endif //PROJEKT_ZOO_2020_SPECIALNIPREDMETY_H
