//
// Created by david on 23.12.2020.
//

#ifndef PROJEKT_ZOO_2020_INVENTORY_H
#define PROJEKT_ZOO_2020_INVENTORY_H
#include <array>
#include <vector>
#include "Lektvar.h"
#include "SpecialniPredmety.h"
#include "ProdejneItemy.h"
#include "../MetodyPlayer.h"

class Inventory {
    std::array<Lektvar*,4> m_lektvary;
    std::vector<SpecialniPredmety*> m_specPredmety;
    std::array<ProdejneItemy*, 5> m_prodejneItemy;
public:
    Inventory();
    void printObsah();
    void printLekvary();
    void printSpecialniPredmety();
    void printProdejneItemy();
    void pridatLektvar(Lektvar* lektvar);
    void odhoditLektvar(int ktery);
    void pouzitLektvar(int ktery);
    void mocLektvaru(Lektvar* lektvar);
    void pridatSpecPredmet(SpecialniPredmety* predmet);
    void pouzitSpecPredmet(int ktery);
    void serazeniSpecPredmetu();
    std::string getKod(int ktery);
    void zahoditProdejnyItem(int ktery);
    void pridatProdejnyItem(ProdejneItemy* item);
    void mocProdejnychItemu(ProdejneItemy* item);
    void vyprazdneniLektvaru();
    void vyprazdneniPrItemu();

};


#endif //PROJEKT_ZOO_2020_INVENTORY_H
