//
// Created by Jan Mecerod on 29.11.2020.
//

#ifndef PROJEKT_ZOO_2020_ZBRAN_H
#define PROJEKT_ZOO_2020_ZBRAN_H
#include <iostream>

class Zbran{
private:
    std::string m_jmeno;
    std::string m_vzacnost;
    float m_bonusUtoku;
public:
    Zbran(std::string vzacnost, std::string jmeno, float bonusUtoku);
    ~Zbran();
    std::string getJmeno();
    std::string getVzacnost();
    std::string vyhozeniZInventare();
    float getBonusUtoku();
    int pouzijSe();
    void printInfo();
};


#endif //PROJEKT_ZOO_2020_ZBRAN_H
