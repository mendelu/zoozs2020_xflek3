//
// Created by Jan Mecerod on 29.11.2020.
//

#include "Zbran.h"

Zbran::Zbran(std::string vzacnost, std::string jmeno, float bonusUtoku){
    m_jmeno = jmeno;
    m_vzacnost = vzacnost;
    m_bonusUtoku = bonusUtoku;
}
Zbran::~Zbran(){
    std::cout << "Zahodil jsi " << getJmeno() << std::endl;
}
std::string Zbran::getJmeno(){
    return m_jmeno;
}
std::string Zbran::getVzacnost(){
    return m_vzacnost;
}
float Zbran::getBonusUtoku(){
    return m_bonusUtoku;
}
int Zbran::pouzijSe() {
    return -200;
}
void Zbran::printInfo() {
    std::cout << std::endl;
    std::cout << "Vzacnost: " << getVzacnost() << std::endl;
    std::cout << "Bonus k utoku: " << getBonusUtoku() << std::endl;
}
std::string Zbran::vyhozeniZInventare(){
    return "Zahodil jsi " + getJmeno();
}