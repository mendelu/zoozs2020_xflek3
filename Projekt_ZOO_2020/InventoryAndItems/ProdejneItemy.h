//
// Created by david on 01.01.2021.
//

#ifndef PROJEKT_ZOO_2020_PRODEJNEITEMY_H
#define PROJEKT_ZOO_2020_PRODEJNEITEMY_H
#include <iostream>

class ProdejneItemy {
    std::string m_jmeno;
    int m_cena;
public:
    ProdejneItemy(std::string jmeno, int cena);
    std::string getJmeno();
    int getCena();
    void printInfo();
};


#endif //PROJEKT_ZOO_2020_PRODEJNEITEMY_H
