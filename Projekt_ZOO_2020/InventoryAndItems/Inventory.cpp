//
// Created by david on 23.12.2020.
//

#include "Inventory.h"
Lektvar* zadnyLektvar = new Lektvar("zadna", "Nic", false, 'h', 0);
ProdejneItemy* zadnyPrItem = new ProdejneItemy("Nic", 0);

Inventory::Inventory(){
    vyprazdneniLektvaru();
    vyprazdneniPrItemu();
}
void Inventory::printObsah(){
    std::cout << "Tve lektvary:\n";
    printLekvary();
    if(m_specPredmety.size() == 0){
        std::cout << "\nZatim nemas zadne specialni predmety. Pokracuj v hrani a hledej!\n";
    }else {
        std::cout << "\nTve specialni predmety:\n";
        printSpecialniPredmety();
    }
    std::cout << "\nProdejne itemy:\n";
    printProdejneItemy();
}
void Inventory::printLekvary(){
    for(int i=0; i<m_lektvary.size(); i++){
        std::cout << i << ") " << m_lektvary.at(i)->getInfo();
    }
}
void Inventory::printSpecialniPredmety(){
    for (int i = 0; i < m_specPredmety.size(); i++) {
        std::cout << i << ") " << m_specPredmety.at(i)->getInfo();
    }
}
void Inventory::printProdejneItemy(){
    for(int i=0; i<m_prodejneItemy.size(); i++){
        std::cout << i << ") "; m_prodejneItemy.at(i)->printInfo();
    }
}
void Inventory::pridatLektvar(Lektvar* lektvar){
    bool truefalse=0;
    int poc =0;
    for(int i=0; i<m_lektvary.size(); i++){
        while(truefalse==0) {
            if (m_lektvary.at(i) == zadnyLektvar) {
                m_lektvary.at(i) = lektvar;
                truefalse=1;
                poc++;
            }else if(poc==4 && truefalse==0){
                mocLektvaru(lektvar);
            }
        }
    }
}
void Inventory::odhoditLektvar(int ktery){
    std::cout << "Odhazujes " << m_lektvary.at(ktery)->getJmeno() << "\n";
    m_lektvary.at(ktery) = nullptr;
}
void Inventory::pouzitLektvar(int ktery){
    if(m_lektvary.at(ktery)->getTyp() == 'h'){
        MetodyPlayer::pridatZivot(m_lektvary.at(ktery)->getBonus());
    }else
        if(m_lektvary.at(ktery)->getTyp() == 'd'){
        MetodyPlayer::pridatObranu(m_lektvary.at(ktery)->getBonus());
    }else
        if(m_lektvary.at(ktery)->getTyp() == 's'){
        MetodyPlayer::pridatSilu(m_lektvary.at(ktery)->getBonus());
    }
}
void Inventory::mocLektvaru(Lektvar* lektvar){
    std::cout << "Mas u sebe moc lektvaru!\n";
    printLekvary();
    std::cout << "Chces tento lektvar :" << lektvar->getInfo() << "\n Vymenit za nejaky tvuj stavajici lektvar? a/n:";
    char anone; std::cin >> anone;
    while(anone!= 'a' || 'n'){
        std::cout << "Neplatna hodnota!! Zadej (a)no, nebo (n)e: "; std::cin >> anone;
    }
    int ktery;
    if(anone == 'a'){
        std::cout << "Za ktery? Zadej cislo: "; std::cin >> ktery;
        m_lektvary.at(ktery)->vyhozeniZInventare();
        m_lektvary.at(ktery) = lektvar;
    }else{
        return;
    }

}
void Inventory::pridatSpecPredmet(SpecialniPredmety* predmet){
    m_specPredmety.push_back(predmet);
}
void Inventory::pouzitSpecPredmet(int ktery){
    int puvodniVelikost = m_specPredmety.size();
    std::cout << "Pouzil jsi " << m_specPredmety.at(ktery)->getJmeno() << std::endl;
    m_specPredmety.at(ktery) = nullptr;
    int i = ktery;
    while(i < puvodniVelikost-1){
        m_specPredmety.at(i) = m_specPredmety.at(i+1);
        i++;
    }
    m_specPredmety.pop_back();
}
void Inventory::serazeniSpecPredmetu(){

}
std::string Inventory::getKod(int ktery){
    return m_specPredmety.at(ktery)->getKod();
}
void Inventory::vyprazdneniLektvaru(){
    for(int i=0; i<m_lektvary.size(); i++){
        m_lektvary.at(i) = zadnyLektvar;
    }
}
void Inventory::vyprazdneniPrItemu(){
    for(int i=0; i<m_prodejneItemy.size(); i++){
        m_prodejneItemy.at(i) = zadnyPrItem;
    }
}
void Inventory::zahoditProdejnyItem(int ktery){
    std::cout << "Zahazujes " << m_prodejneItemy.at(ktery)->getJmeno() << " v hodnote " << m_prodejneItemy.at(ktery)->getCena();
    m_prodejneItemy.at(ktery) = zadnyPrItem;
}
void Inventory::pridatProdejnyItem(ProdejneItemy* item){
    bool p=false;
    while(p==false) {
        for (int i = 0; i < m_prodejneItemy.size(); i++) {
            if (m_prodejneItemy.at(i) == zadnyPrItem) {
                m_prodejneItemy.at(i) = item;
                p=true;
            }
        }
    }
}
void Inventory::mocProdejnychItemu(ProdejneItemy* item){

}