//
// Created by Jan Mecerod on 29.11.2020.
//

#ifndef PROJEKT_ZOO_2020_LEKTVAR_H
#define PROJEKT_ZOO_2020_LEKTVAR_H
#include <iostream>

class Lektvar{
    std::string m_jmeno;
    std::string m_vzacnost;
    bool m_shiny;
    float m_bonus;
    char m_typ; // 'h'=heal, 's'=strength, 'd'=defence
public:
    Lektvar(std::string vzacnost, std::string jmeno, bool shiny, char typ, float bonus);
    ~Lektvar();
    std::string vyhozeniZInventare();
    std::string getJmeno();
    std::string getVzacnost();
    bool getShiny();
    float getBonus();
    char getTyp();
    int vypit();
    std::string getInfo();
};




#endif //PROJEKT_ZOO_2020_LEKTVAR_H
