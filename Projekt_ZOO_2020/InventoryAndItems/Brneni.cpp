//
// Created by Jan Mecerod on 29.11.2020.
//

#include "Brneni.h"

Brneni::Brneni(std::string vzacnost, std::string jmeno, float bonusObrany){
    m_jmeno = jmeno;
    m_vzacnost = vzacnost;
    m_bonusObrany = bonusObrany;
}
Brneni::~Brneni(){
    std::cout << "Zahodil jsi " << getJmeno() << std::endl;
}
std::string Brneni::getJmeno(){
    return m_jmeno;
}
std::string Brneni::getVzacnost(){
    return m_vzacnost;
}
float Brneni::getBonusObrany() {
    return m_bonusObrany;
}
int Brneni::pouzijSe() {
    return -100;
}
void Brneni::printInfo() {
    std::cout << std::endl;
    std::cout << getJmeno() << std::endl;
    std::cout << "Vzacnost: " << getVzacnost() << std::endl;
    std::cout << "Bonus k obrane: " << getBonusObrany() << std::endl;
}
std::string Brneni::vyhozeniZInventare(){
    return "Zahodil jsi " + getJmeno();
}