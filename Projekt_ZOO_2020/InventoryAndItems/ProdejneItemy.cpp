//
// Created by david on 01.01.2021.
//

#include "ProdejneItemy.h"
ProdejneItemy::ProdejneItemy(std::string jmeno, int cena){
    m_jmeno = jmeno;
    m_cena = cena;
}
std::string ProdejneItemy::getJmeno(){
    return m_jmeno;
}
int ProdejneItemy::getCena(){
    return m_cena;
}
void ProdejneItemy::printInfo(){
    std::cout << getJmeno() << " Cena: " << getCena() << std::endl;
}