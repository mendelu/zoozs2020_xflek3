//
// Created by Jan Mecerod on 29.11.2020.
//

#ifndef PROJEKT_ZOO_2020_BRNENI_H
#define PROJEKT_ZOO_2020_BRNENI_H
#include <iostream>

class Brneni{
private:
    std::string m_jmeno;
    std::string m_vzacnost;
    float m_bonusObrany;
public:
    Brneni(std::string vzacnost, std::string jmeno, float bonusObrany);
    ~Brneni();
    std::string getJmeno();
    std::string getVzacnost();
    float getBonusObrany();
    int pouzijSe();
    void printInfo();
    std::string vyhozeniZInventare();
};


#endif //PROJEKT_ZOO_2020_BRNENI_H
