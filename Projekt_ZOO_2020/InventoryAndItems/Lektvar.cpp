//
// Created by Jan Mecerod on 29.11.2020.
//

#include "Lektvar.h"

Lektvar::Lektvar(std::string vzacnost, std::string jmeno, bool shiny, char typ, float bonus){
    m_jmeno = jmeno;
    m_vzacnost = vzacnost;
    m_typ = typ;
    m_bonus = bonus;
    m_shiny = shiny;
}
Lektvar::~Lektvar() {
    std::cout << "\nVypil jsi " << getJmeno() << std::endl;
}
std::string Lektvar::getJmeno(){
    return m_jmeno;
}
std::string Lektvar::getVzacnost(){
    return m_vzacnost;
}
float Lektvar::getBonus() {
    return m_bonus;
}

char Lektvar::getTyp(){
    return m_typ;
}
bool Lektvar::getShiny(){
    return m_shiny;
}
int Lektvar::vypit() {
    if(getTyp() == 'h'){
        return 100 + getBonus();
    }else if(getTyp() == 's'){
        return 200 + getBonus();
    }else
        return 300 + getBonus();
}
std::string Lektvar::getInfo() {
    std::string jeShiny = "ne";
    if(getShiny()==true){
        jeShiny = "ano";
    }
    return getJmeno() + "\tVzacnost: " + getVzacnost() + "\tShiny: " + jeShiny + "\n";
}
std::string Lektvar::vyhozeniZInventare(){
    return "Zahodil jsi " + getJmeno();
}

