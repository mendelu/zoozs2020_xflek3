//
// Created by Jan Mecerod on 06.12.2020.
//

#ifndef PROJEKT_ZOO_2020_TEZKYKENTAUR_H
#define PROJEKT_ZOO_2020_TEZKYKENTAUR_H
#include "Kentaur.h"

class TezkyKentaur: public Kentaur {
    int m_silaUtoku;
public:
    int getSilaUtoku();
    TezkyKentaur(int silaUtoku);
};


#endif //PROJEKT_ZOO_2020_TEZKYKENTAUR_H
