//
// Created by Jan Mecerod on 06.12.2020.
//

#include "TezkyKentaur.h"


TezkyKentaur::TezkyKentaur(int silaUtoku) {
    m_silaUtoku = silaUtoku;
}

int TezkyKentaur::getSilaUtoku() {
    return 30*m_silaUtoku;
}