//
// Created by Jan Mecerod on 03.12.2020.
//

#ifndef PROJEKT_ZOO_2020_TEZKYSKELETON_H
#define PROJEKT_ZOO_2020_TEZKYSKELETON_H
#include "Kyklop.h"

class TezkyKyklop: public Kyklop {
    int m_silaUtoku;
public:
    int getSilaUtoku();
    TezkyKyklop(int silaUtoku);
};


#endif //PROJEKT_ZOO_2020_TEZKYSKELETON_H
