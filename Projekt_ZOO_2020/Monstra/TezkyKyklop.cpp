//
// Created by Jan Mecerod on 03.12.2020.
//

#include "TezkyKyklop.h"

TezkyKyklop::TezkyKyklop(int silaUtoku) {
    m_silaUtoku = silaUtoku;
}

int TezkyKyklop::getSilaUtoku() {
    return 30*m_silaUtoku;
}