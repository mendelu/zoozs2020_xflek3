//
// Created by Jan Mecerod on 03.12.2020.
//

#ifndef PROJEKT_ZOO_2020_TOVARNAMONSTER_H
#define PROJEKT_ZOO_2020_TOVARNAMONSTER_H
#include <iostream>
#include "Kyklop.h"

class TovarnaMonster {
public:
    virtual Kyklop* getKyklop() = 0;
};


#endif //PROJEKT_ZOO_2020_TOVARNAMONSTER_H
