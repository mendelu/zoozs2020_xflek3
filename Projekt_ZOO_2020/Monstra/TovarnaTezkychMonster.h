//
// Created by Jan Mecerod on 03.12.2020.
//

#ifndef PROJEKT_ZOO_2020_TOVARNATEZKYCHMONSTER_H
#define PROJEKT_ZOO_2020_TOVARNATEZKYCHMONSTER_H

#include "TovarnaMonster.h"
#include "TezkyKyklop.h"

class TovarnaTezkychMonster: public TovarnaMonster{
public:
    Kyklop* getKyklop();
};

#endif //PROJEKT_ZOO_2020_TOVARNATEZKYCHMONSTER_H
