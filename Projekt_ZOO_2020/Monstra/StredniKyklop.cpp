//
// Created by Jan Mecerod on 03.12.2020.
//

#include "StredniKyklop.h"

StredniKyklop::StredniKyklop(int silaUtoku) {
    m_silaUtoku = silaUtoku;
}

int StredniKyklop::getSilaUtoku() {
    return 20*m_silaUtoku;
}