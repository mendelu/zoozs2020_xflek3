//
// Created by Jan Mecerod on 06.12.2020.
//

#ifndef PROJEKT_ZOO_2020_KENTAUR_H
#define PROJEKT_ZOO_2020_KENTAUR_H


class Kentaur {
public:
    virtual int getSilaUtoku() = 0;
    virtual ~Kentaur(){};
    Kentaur(){}
};


#endif //PROJEKT_ZOO_2020_KENTAUR_H
