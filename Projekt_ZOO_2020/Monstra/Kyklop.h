//
// Created by Jan Mecerod on 03.12.2020.
//

#ifndef PROJEKT_ZOO_2020_SKELETON_H
#define PROJEKT_ZOO_2020_SKELETON_H


class Kyklop {
public:
    virtual int getSilaUtoku() = 0;
    virtual ~Kyklop(){};
    Kyklop(){}
};


#endif //PROJEKT_ZOO_2020_SKELETON_H
