//
// Created by Jan Mecerod on 03.12.2020.
//

#ifndef PROJEKT_ZOO_2020_STREDNISKELETON_H
#define PROJEKT_ZOO_2020_STREDNISKELETON_H
#include "Kyklop.h"

class StredniKyklop: public Kyklop {
    int m_silaUtoku;
public:
    int getSilaUtoku();
    StredniKyklop(int silaUtoku);
};


#endif //PROJEKT_ZOO_2020_STREDNISKELETON_H
