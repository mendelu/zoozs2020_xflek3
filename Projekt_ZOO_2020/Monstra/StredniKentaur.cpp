//
// Created by Jan Mecerod on 06.12.2020.
//

#include "StredniKentaur.h"

StredniKentaur::StredniKentaur(int silaUtoku) {
    m_silaUtoku = silaUtoku;
}

int StredniKentaur::getSilaUtoku() {
    return 20*m_silaUtoku;
}