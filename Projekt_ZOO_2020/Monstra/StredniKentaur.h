//
// Created by Jan Mecerod on 06.12.2020.
//

#ifndef PROJEKT_ZOO_2020_STREDNIKENTAUR_H
#define PROJEKT_ZOO_2020_STREDNIKENTAUR_H
#include "Kentaur.h"

class StredniKentaur: public Kentaur {
    int m_silaUtoku;
public:
    int getSilaUtoku();
    StredniKentaur(int silaUtoku);
};


#endif //PROJEKT_ZOO_2020_STREDNIKENTAUR_H
