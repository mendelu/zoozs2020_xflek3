//
// Created by Jan Mecerod on 03.12.2020.
//

#ifndef PROJEKT_ZOO_2020_TOVARNASTREDNICHMONSTER_H
#define PROJEKT_ZOO_2020_TOVARNASTREDNICHMONSTER_H

#include "TovarnaMonster.h"
#include "StredniKyklop.h"

class TovarnaStrednichMonster: public TovarnaMonster{
public:
    Kyklop* getKyklop();
};

#endif //PROJEKT_ZOO_2020_TOVARNASTREDNICHMONSTER_H
