//
// Created by david on 20.11.2020.
//

#include "Player.h"

Player::Player(std::string jmeno){

    Zbran* klacek = new Zbran("common", "Klacek", 1);
    Pet* kruta = new Pet("Kruta", 5, 0.1, 1);
    Brneni* tricko = new Brneni("common","Tricko", 1);
    m_jmeno = jmeno;
    m_zdravi = 100;
    m_zlataky = 0.0;
    m_zviratko = kruta;
    m_brneni = tricko;
    m_zbran = klacek;
    m_obrana = 10 + m_brneni->getBonusObrany();
    m_sila = 10 + m_zbran->getBonusUtoku();
}

// O PLAYEROVI
std::string Player::getJmeno(){
    return m_jmeno;
}
void Player::setJmeno(std::string jmeno){
    m_jmeno = jmeno;
}
float Player::getZdravi(){
    return m_zdravi;
}
void Player::setZdravi(int kolik){
    m_zdravi += kolik;
}
float Player::getSila(){
    return m_sila;
}
void Player::setSila(int kolik){
    m_sila += kolik;
}
float Player::getObrana(){
    return m_obrana;
}
void Player::setObrana(int kolik) {
    m_obrana += kolik;
}
float Player::getZlataky(){
    return m_zlataky;
}

Pet* Player::getZviratko(){
    //if(m_zviratko!= nullptr){
        return m_zviratko;
    //}else return "Vypadas osamele, kup si alespon kocku.";
}

Brneni* Player::getBrneni(){
    return m_brneni;
}
Zbran* Player::getZbran(){
    return m_zbran;
}

void Player::printInfo(){
    std::cout << "____________ \n";
    std::cout << "Jmeno hrdiny: " << getJmeno() << std::endl;
    std::cout << "Zivoty: " << getZdravi() << std::endl;
    std::cout << "Sila: " << getSila() << std::endl;
    std::cout << "Obrana: " << getObrana() << std::endl;
    std::cout << "Pet: " << getZviratko()->getJmeno() << std::endl;
    std::cout << "Cash: "; vypisZlataky();
    std::cout << std::endl;
}
void Player::vypisZlataky(){
    std::string celkem = "";
    float zlataky = m_zlataky;
    int pretypovaniZlata;
    pretypovaniZlata = (float)zlataky;
    float stribro = (zlataky - pretypovaniZlata)*100;
    int pretypovaniStribra;
    pretypovaniStribra = (int)stribro;
    std::cout << "Zlataky: " << pretypovaniZlata << ", Stribro: " << pretypovaniStribra << std::endl;
}
// ITEMY

void Player::nasadBrneni(Brneni* brneni){
    if(m_brneni != nullptr){
        std::cout << "Již máš nasazene jedno brneni. Chces jej vymenit za tohle? [a]no nebo [n]e?" << std::endl;
        char anone;
        std::cin >> anone;
        if(anone == 'a'){
            zahodBrneni();
            m_brneni = brneni;
            m_obrana += brneni->Brneni::getBonusObrany();
        }
    }
}

void Player::zahodBrneni(){
    m_obrana -= m_brneni->getBonusObrany();
    m_brneni = nullptr;
}


void Player::nasadZbran(Zbran* zbran){
    if(m_zbran != nullptr){
        std::cout << "Jiz mas v ruce  jednu zbran. Chces ji vymenit za tuhle? [a]no nebo [n]e?" << std::endl;
        char anone;
        std::cin >> anone;
        if(anone == 'a'){
            zahodZbran();
            m_zbran = zbran;
            m_sila += zbran->Zbran::getBonusUtoku();
        }
    }
}
void Player::zahodZbran(){
    m_sila -= m_zbran->getBonusUtoku();
    m_zbran = nullptr;
}

void Player::sundejBrneni() {
    m_obrana -= m_brneni->getBonusObrany();
    m_brneni = nullptr;
}
