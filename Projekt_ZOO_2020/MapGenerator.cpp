//
// Created by david on 06.12.2020.
//

#include "MapGenerator.h"
std::string MapGenerator::mapaSpawn(){
    std::string m_mapa = "";
    m_mapa = "/--------------------------------------------------------------------------------------------------------------------\\\n";
    m_mapa += "|                                                                                                                    |\n";
    m_mapa += "|       |                                                                      ^                                     |\n";
    m_mapa += "|       |                                                        ^        ^   / \\                                    |\n";
    m_mapa += "|      \\|/                                                   ^  / \\  /---/ \\--   \\                                   |\n";
    m_mapa += "|       V                                                ^  / \\/   ----    /      \\---\\                              |\n";
    m_mapa += "|     _________                                         / \\/   \\  /    \\  /            \\                             |\n";
    m_mapa += "|    /*********\\                                       /   \\     /      \\               \\                            |\n";
    m_mapa += "|   /***********\\                                     /     \\    | HORY |---                                         |\n";
    m_mapa += "|  /*************\\                                               |      |###\\--                                      |\n";
    m_mapa += "| /|             |\\                                              \\      /-\\####\\--                                   |\n";
    m_mapa += "|  |    spawn    |                                                \\    /   --\\####\\--                                |\n";
    m_mapa += "|  |             |                                                 ----       --\\####\\--------------                 |\n";
    m_mapa += "|  |             |                                                               --\\################\\                |\n";
    m_mapa += "|  ---------------                                                                  --------------\\##\\--   /\\        |\n";
    m_mapa += "|        |#|                                                                                       \\####\\ /||\\       |\n";
    m_mapa += "|        |#|                                                                                    /\\  --\\#| /||\\       |\n";
    m_mapa += "|         \\#\\                                                                                  /||\\   |#|  ||        |\n";
    m_mapa += "|          \\#\\--------------                                                                   /||\\  -----    /\\     |\n";
    m_mapa += "|           \\###############\\---                                                                ||  /     \\  /||\\    |\n";
    m_mapa += "|            --------------\\####\\             -------                                              /       \\ /||\\    |\n";
    m_mapa += "|                           ---\\#\\-          /       \\                                          ---|  LES  |  ||     |\n";
    m_mapa += "|                               \\##\\-       /         \\                                      --/###|       |         |\n";
    m_mapa += "|                                -\\##\\---  /           \\                                  --/####/-\\       /         |\n";
    m_mapa += "|                                  -\\####\\-|           |--------                       --/####/--   \\     /          |\n";
    m_mapa += "|                                    ---\\##|  NAMESTI  |########\\----              ---/####/--       -----   /\\      |\n";
    m_mapa += "|                                        --|           |------\\######\\------------/####/---                 /||\\     |\n";
    m_mapa += "|                                          \\           /       ----\\################/--           /\\    /\\  /||\\     |\n";
    m_mapa += "|                                           \\         /             ----------------             /||\\  /||\\  ||      |\n";
    m_mapa += "|                                            \\       /                                           /||\\  /||\\          |\n";
    m_mapa += "|                                             -------                                             ||    ||           |\n";
    m_mapa += "|                                               |#|                                                                  |\n";
    m_mapa += "|                                               |#|                                                              ____|\n";
    m_mapa += "|                                               |#\\-                                                          __/    |\n";
    m_mapa += "|                                                \\##\\-                                                   ____/       |\n";
    m_mapa += "|                                                 \\###\\---         ------                           ____/            |\n";
    m_mapa += "|                                                  -\\#####\\-------/######\\---                ______/                 |\n";
    m_mapa += "|                                                    ---\\###########/--\\#####\\--            /              ~~~~~     |\n";
    m_mapa += "|                                                        -----------    ---\\####\\-        --              ~~   ~~~~~ |\n";
    m_mapa += "|                                                                           --\\###\\-    _/    PRISTAV   ~~~          |\n";
    m_mapa += "|                                                                              --\\##\\- /                             |\n";
    m_mapa += "|                                                                                 \\###/                              |\n";
    m_mapa += "|                                                                                  --/                   ~~~~~~~~~   |\n";
    m_mapa += "|                                                                                 _/      ---       ~~~~~~~~~        |\n";
    m_mapa += "|                                                                               _/       /*  \\/|   ~~                |\n";
    m_mapa += "|                                                                              /         &   /\\|                     |\n";
    m_mapa += "|                                                                             /          \\---              ~~~~      |\n";
    m_mapa += "|                                                                            /                          ~~~~         |\n";
    m_mapa += "|                                                                                                                    |\n";
    m_mapa += "\\--------------------------------------------------------------------------------------------------------------------/";
    return m_mapa;
}