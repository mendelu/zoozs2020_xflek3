//
// Created by david on 20.11.2020.
//

#ifndef MAIN_CPP_LOKACE_H
#define MAIN_CPP_LOKACE_H
#include <iostream>
#include <iomanip>
#include <vector>
#include "../NPC/Npc.h"
#include "../Monstra/TovarnaMonster.h"
#include "Mista.h"
#include "VychoziMisto.h"
class Lokace {
protected:
    std::string m_jmeno;
    std::string m_mapa;
    std::vector<Mista*> m_mista;
    std::vector<Lokace*> m_sousedi;
public:
    Lokace(std::string jmeno, std::string mapa);
    std::string getJmeno();
     void zobrazMapu();
     void porozhledniSePoOkoli(int vynechat);
     Mista* getMista(int i);
     int getPocetMist();
     int najdiCisloMista(Mista* misto);
     void pridatMisto(Mista* misto);
     void pridatSouseda(Lokace* soused);
};


#endif //MAIN_CPP_LOKACE_H
