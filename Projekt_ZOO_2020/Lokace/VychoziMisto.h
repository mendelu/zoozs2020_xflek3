//
// Created by david on 22.12.2020.
//

#ifndef PROJEKT_ZOO_2020_VYCHOZIMISTO_H
#define PROJEKT_ZOO_2020_VYCHOZIMISTO_H
#include "Mista.h"

class VychoziMisto:public Mista {
public:
    VychoziMisto(std::string jmeno, std::string popis);
    void coTu();
};


#endif //PROJEKT_ZOO_2020_VYCHOZIMISTO_H
