//
// Created by david on 05.12.2020.
//


#ifndef PROJEKT_ZOO_2020_MISTA_H
#define PROJEKT_ZOO_2020_MISTA_H
#include <iostream>
#include "../InventoryAndItems/Zbran.h"
#include "../InventoryAndItems/Brneni.h"
#include "../InventoryAndItems/Lektvar.h"
#include "../Monstra/Kyklop.h"
#include "../NPC/Pet.h"
#include "../NPC/Npc.h"
#include "../MetodyPlayer.h"
class Mista {
    protected:
        std::string m_jmeno;
        std::string m_popis;
        bool m_hotovo;
    public:
        Mista(std::string jmeno, std::string popis);
        std::string getJmeno();
        std::string getPopis();
        bool getHotovo();
        virtual void coTu() = 0;

};


#endif //PROJEKT_ZOO_2020_MISTA_H
