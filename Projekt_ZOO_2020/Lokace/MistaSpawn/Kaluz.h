//
// Created by david on 22.12.2020.
//

#ifndef PROJEKT_ZOO_2020_KALUZ_H
#define PROJEKT_ZOO_2020_KALUZ_H
#include "../Mista.h"

class Kaluz: public Mista {
public:
    Kaluz(std::string jmeno, std::string popis);
    void coTu();
};


#endif //PROJEKT_ZOO_2020_KALUZ_H
