//
// Created by david on 20.11.2020.
//

#include "Lokace.h"
VychoziMisto* vychoziBod = nullptr;
Lokace::Lokace(std::string jmeno, std::string mapa){
    if(vychoziBod == nullptr){
        vychoziBod = new VychoziMisto("Vychozi bod", "Popis");
    }
    m_jmeno = jmeno;
    m_mapa = mapa;
    m_mista.push_back(vychoziBod);
}
void Lokace::porozhledniSePoOkoli(int vynechat){
    std::cout << "\nVe svem okoli vidis nasledujici:\n";
    for(int i=0; i<m_mista.size(); i++){
        if(i != vynechat){
            std::cout << i << ":\t" << m_mista.at(i)->getJmeno() << std::endl;
        }
    }
}
int Lokace::najdiCisloMista(Mista* misto){
    int pocitadlo = -1;
    for(int i=0; i<m_mista.size(); i++){
        pocitadlo++;
        if(m_mista.at(i) == misto){
            i=m_mista.size();
        }
    }
    return pocitadlo;
}
void Lokace::zobrazMapu(){
    std::cout << m_mapa;
}
std::string Lokace::getJmeno() {
    return m_jmeno;
}

int Lokace::getPocetMist(){
    return m_mista.size();
}
Mista* Lokace::getMista(int i){
    return m_mista.at(i);
}
void Lokace::pridatMisto(Mista* misto){
    m_mista.push_back(misto);
}
void Lokace::pridatSouseda(Lokace* soused){
    m_sousedi.push_back(soused);
}