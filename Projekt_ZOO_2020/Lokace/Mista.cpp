//
// Created by david on 05.12.2020.
//

#include "Mista.h"


Mista::Mista(std::string jmeno, std::string popis){
    m_jmeno = jmeno;
    m_popis = popis;
    m_hotovo = false;
}
std::string Mista::getJmeno(){
    return m_jmeno;
}
std::string Mista::getPopis(){
    return m_popis;
}
bool Mista::getHotovo(){
    return m_hotovo;
}