//
// Created by david on 20.11.2020.
//

#ifndef MAIN_CPP_PLAYER_H
#define MAIN_CPP_PLAYER_H
#include <iomanip>
#include "NPC/Pet.h"
#include "InventoryAndItems/Brneni.h"
#include "InventoryAndItems/Zbran.h"
#include "InventoryAndItems/Inventory.h"

class Player {
    std::string m_jmeno;
    float m_zdravi;
    float m_sila;
    float m_obrana;
    float m_zlataky;
    Pet* m_zviratko;
    Brneni* m_brneni;
    Zbran* m_zbran;

public:
    /**********ZÁKLAD**********/
    Player(std::string jmeno);
    void printInfo();
    /**********INVENTAR**********/
    void printInventar();
    bool fullInvent();
    void emptyInvent();
    void odstranItem(int pozice);
    void pouzitItem(int pozice);
    void vypisZlataky();
    /**********GETTERY**********/
    std::string getJmeno();
    float getZdravi();
    float getSila();
    float getObrana();
    float getZlataky();
    Pet* getZviratko();
    Brneni* getBrneni();
    Zbran* getZbran();

    /**********SETTERY**********/
    void setJmeno(std::string jmeno);
    void setZdravi(int kolik);
    void setSila(int kolik);
    void setObrana(int kolik);
    /**********NASAĎ X ZAHOĎ**********/
    void nasadBrneni(Brneni* brneni);
    void zahodBrneni();
    void nasadZbran(Zbran* zbran);
    void zahodZbran();
    void sundejBrneni();
};


#endif //MAIN_CPP_PLAYER_H
