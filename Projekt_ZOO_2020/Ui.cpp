//
// Created by david on 20.11.2020.
//

#include "Ui.h"
/*
Lokace* Ui::s_aktualniLokaceHrace = nullptr;
Mista* Ui::s_aktualniMistoHrace = nullptr;
*/
Lektvar* HealingLektvar = new Lektvar("rare", "HealingLektvar", 1, 'h', 50);
Lektvar* StrengthLektvar = new Lektvar("rare", "StrengthLektvar", 1, 's', 20);
Lektvar* ArmorLektvar = new Lektvar("rare", "ArmorLektvar", 1, 'd', 20);
SpecialniPredmety* TestovaciKlic = new SpecialniPredmety("test", "rare", "666");
Ui::Ui(){
    Inventory* inventar = new Inventory();
    m_inventar = inventar;
    vygenerovaniLokaci();
}
void Ui::prvniZapnutiHry(){
    std::cout << "Vitej v Jumanji! Nejdrive si zvol jmeno vyvolavaci!" << std::endl;
    std::string jmeno;
    getline(std::cin, jmeno);
    MetodyPlayer::prvniNastaveniHrace(jmeno);
    m_inventar->pridatLektvar(HealingLektvar);
    m_inventar->pridatSpecPredmet(TestovaciKlic);
    m_inventar->pridatSpecPredmet(TestovaciKlic);
    zvoleniObtiznosti();
    coTed();
}

void Ui::zvoleniObtiznosti(){
    std::cout << "Ted si zvol obtiznost. Pozor, v prubehu hry ji jiz nebudes mit moznost zmenit, vybirej tedy s rozmyslem!" << std::endl;
    std::cout << "1 = Hodne tezka, 2 = impossibruh" << std::endl;
    std::string obtiznost;
    std::cin >> obtiznost;
    TovarnaMonster* tovarnaMonster = nullptr;
    //podminka pro generovani monster podle uzivatelem zvolene obtiznosti na zacatku
    if (obtiznost == "1") {
        tovarnaMonster = new TovarnaStrednichMonster();
    } else if (obtiznost == "2") {
        tovarnaMonster = new TovarnaTezkychMonster();
    }
    if (obtiznost != "1") {
        if (obtiznost != "2") {
            zvoleniObtiznosti();
        }
    }
    else {
        //Tady tohle by se asi melo dat do metod vsech lokaci, ktery budeme teda mit, aby v kazde lokaci byly nejaky monstra.
        //Muzeme tem monstrum dat taky nejaky zivoty a nebo proste po jednom utoku hrace chcipnou - to by potom delala ta
        //obtiznost rozdil jen v tom, kdyby na hrace zautocilo vic monster najednou, tak by ho mohli zabit nebo donutit pouzit
        //lektvar.
        Kyklop *kyklop = tovarnaMonster->getKyklop();
    }
}
void Ui::vypisMoznosti(){
    std::cout << "\nCo ted?" << std::endl;
    std::cout << "____________________" << std::endl;
    std::cout << "0 ... Vypnout Hru" << std::endl;
    std::cout << "1 ... Otevrit mapu" << std::endl;
    std::cout << "2 ... Zkontrolovat svuj stav" << std::endl;
    std::cout << "3 ... Porozhlednout se po okoli" << std::endl;
    std::cout << "4 ... Pohladit Zviratko" << std::endl;
    std::cout << "5 ... Neco dalsiho" << std::endl;
}
void Ui::co1(){ //otevřít mapu
    s_aktualniLokaceHrace->zobrazMapu();

    coTed();
}
void Ui::co2(){ //Zkontrolovat svůj stav
    MetodyPlayer::printInfo();
    std::cout << "Chces videt svuj inventar? [a]no nebo [n]e?\n";
    char anone;
    std::cin >> anone;
    if(anone == 'a'){
        m_inventar->printObsah();
    }
    else{
        coTed();
    }
    //std::cout << "Chces neco [v]yhodit, [p]ouzit, nebo [o]pustit inventar?\n";
    char nabidka;
    //std::cin >> nabidka;
    coTed();
}
void Ui::co3(){ // Porozhlédnout se po okolí
    porozhledniSe();
    std::cout << "\nKam chces jit? (pro odchod z nabidky zadej 'o'): ";
    std::string vstup;
    std::cin >> vstup;
    int velikost = pocetMistVLokaci();
    while (vstup != "o" && vstup != "0" && vstup != "1" && vstup != "2" && vstup != "3" && vstup != "4") {
        overovaniVstupuProPorozhlednutiPoOkoli();
    }
    if (vstup == "o") {
        Ui::coTed();
    }
    if (vstup == "0") {
        zmenMisto(0);
    }
    if (vstup == "1") {
        zmenMisto(1);
    }
    if (vstup == "2") {
        zmenMisto(2);
    }
    if (vstup == "3") {
        zmenMisto(3);
    }
    if (vstup == "4") {
        zmenMisto(4);
    }
    s_aktualniMistoHrace->coTu();
}
void Ui::co4(){ //Pohladit zvířátko
    MetodyPlayer::pohladZviratko();
    coTed();
}
void Ui::co5(){
    //volno,
    m_inventar->pridatLektvar(HealingLektvar);
    coTed();
}
void Ui::coTed() {
    vypisMoznosti();
    std::string co = "";
    std::cin >> co;
    if(co >="0" && co <="5") {
        while (co != "0") {
            if (co == "1") {
                co1();
            } else if (co == "2") {
                co2();
            } else if (co == "3") {
                co3();
            } else if (co == "4") {
                co4();
            } else if (co == "5") {
                co5();
            } else
                coTed();
        }
        if (co == "0") {
            kontrolaVypnutiHry();
        }
    } else{
        std::cout << "Pod timto znakem neni zadna akce, zkus to znovu!\n";
        coTed();
    }
}
void Ui::coTu(){
    if(getHotoveMisto() != true){

    }else{
        std::cout << "\nTuto oblast jsi jiz prozkoumal\n";
        coTed();
    }
}
void Ui::kontrolaVypnutiHry(){
    std::cout << "Opravdu chcete hru vypnout? [a]no nebo [n]e \n";
    char anone;
    std::cin >> anone;
    if(anone == 'a'){
        std::cout << "Diky za hrani nasi hry!! :)";
        vypnutiHry();
    }else{
        std::cout << "Spravna volba!!";
        coTed();
    }
}

void Ui::vypnutiHry(){
    exit (0);
}

void Ui::vypisZlataky(float pocetZlataku) {
    std::string celkem = "";
    float zlataky = pocetZlataku;
    int pretypovaniZlata;
    pretypovaniZlata = (float)zlataky;
    float stribro = (zlataky - pretypovaniZlata)*100;
    int pretypovaniStribra;
    pretypovaniStribra = (int)stribro;
    std::cout << "Zlataky: " << pretypovaniZlata << ", Stribro: " << pretypovaniStribra << std::endl;
}

void Ui::overovaniVstupuProPorozhlednutiPoOkoli(){
    porozhledniSe();
    std::cout << "\nKam chces jit? (pro odchod z nabidky zadej 'o'): ";
    std::string vstup;
    std::cin >> vstup;
    int velikost = pocetMistVLokaci();
}
void Ui::vygenerovaniLokaci(){
    // spawn
    Kaluz* kaluz = new Kaluz("Kaluz", "Popis");
    /*Mista* Parez = new Mista("Kouzelny parez", "popis parezu");
    Mista* Cedule = new Mista("Cedule", "rozcestnik");
    Mista* spawnmesto = new Mista("Cesta na vychod", "Prapodivna cesta");
    Mista* penezenka = new Mista("Penezenka", "Penezenka na strome");*/
    Lokace* spawn = new Lokace("Spawn", MapGenerator::mapaSpawn());
    spawn->pridatMisto(kaluz);//spawn->pridatMisto(Parez); spawn->pridatMisto(Cedule); spawn->pridatMisto(penezenka);spawn->pridatMisto(spawnmesto);
    s_aktualniLokaceHrace = spawn;
    s_aktualniMistoHrace = spawn->getMista(0);
    // mesto
    /*Mista* obchod = new Mista("Obchod se smisenym zbozim", "Vypada ze se kazdou chvili ropadne");
    Mista* carodej = new Mista("Carodejovo doupe", "Proste dira");
    Mista* kostel = new Mista("Kostel sv. Jarmila", "kostel");
    Mista* centurm = new Mista("Rusne centurm", "proste centurm");
    Mista* mestospawn = new Mista("Cesta na severo-zapad", "Zase ta divna cesta");
    Lokace* mesto = new Lokace("Mesto", MapGenerator::mapaSpawn());
    mesto->pridatMisto(obchod);mesto->pridatMisto(carodej);mesto->pridatMisto(kostel);mesto->pridatMisto(centurm);mesto->pridatMisto(mestospawn);

    //Les
    Mista* prastaraJedle = new Mista("Prastara jedle", "Jedle starsi nez vesmir sam");
    Lokace* les = new Lokace("Zapovezeny les", MapGenerator::mapaSpawn());
    les->pridatMisto(prastaraJedle);

    //přidání všech sousedů
    spawn->pridatSouseda(mesto);
    mesto->pridatSouseda(spawn); mesto->pridatSouseda(les);*/
}
void Ui::porozhledniSe(){
    s_aktualniLokaceHrace -> porozhledniSePoOkoli(ziskejCisloPozice());
}
int Ui::ziskejCisloPozice(){
    return s_aktualniLokaceHrace->najdiCisloMista(s_aktualniMistoHrace);
}
int Ui::pocetMistVLokaci(){
    return s_aktualniLokaceHrace->getPocetMist();
}
void Ui::zmenMisto(int cislo){
    s_aktualniMistoHrace = s_aktualniLokaceHrace->getMista(cislo);
}
bool Ui::getHotoveMisto(){
    return s_aktualniMistoHrace->getHotovo();
}