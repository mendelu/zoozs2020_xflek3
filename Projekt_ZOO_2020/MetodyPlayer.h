//
// Created by david on 08.12.2020.
//

#ifndef PROJEKT_ZOO_2020_METODYPLAYER_H
#define PROJEKT_ZOO_2020_METODYPLAYER_H
#include "InventoryAndItems/Zbran.h"
#include "InventoryAndItems/Brneni.h"
#include "InventoryAndItems/Lektvar.h"
#include "Player.h"

class MetodyPlayer {
public:
    static void prvniNastaveniHrace(std::string jmeno);
    static void odstranItem(int pozice);
    static void pouzitItem(int pozice);
    //static void pridatItem(Item* item);
    static void pridatZivot(int kolik);
    static void pridatSilu(int kolik);
    static void pridatObranu(int kolik);
    static void printInfo();
    static void printInventar();
    static void pohladZviratko();
};


#endif //PROJEKT_ZOO_2020_METODYPLAYER_H
