//
// Created by david on 20.11.2020.
//

#ifndef MAIN_CPP_PET_H
#define MAIN_CPP_PET_H
#include "Npc.h"

class Pet: public Npc{
    std::string m_pohlazeni;
public:
    Pet(std::string jmeno, float zdravi, float utok, bool pratelsky);
    void nechSePohladit();
    std::string getJmeno();
};


#endif //MAIN_CPP_PET_H
