//
// Created by david on 20.11.2020.
//

#include "Npc.h"

Npc::Npc(std::string jmeno, float zdravi, float utok, bool pratelsky){
    m_jmeno = jmeno;
    m_zdravi = zdravi;
    m_utok = utok;
    m_pratelsky = pratelsky;
}
// gettery
std::string Npc::getJmeno(){
    return m_jmeno;
}
float Npc::getZdravi(){
    return m_zdravi;
}
float Npc::getUtok(){
    return m_utok;
}
bool Npc::getPratelsky(){
    return m_pratelsky;
}