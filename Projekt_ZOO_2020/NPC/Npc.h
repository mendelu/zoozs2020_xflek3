//
// Created by david on 20.11.2020.
//

#ifndef MAIN_CPP_NPC_H
#define MAIN_CPP_NPC_H
#include <iostream>
class Npc {
protected:
    std::string m_jmeno;
    float m_zdravi;
    float m_utok;
    bool m_pratelsky;

public:
    Npc(std::string jmeno, float zdravi, float utok, bool pratelsky);
    std::string getJmeno();
    float getZdravi();
    float getUtok();
    bool getPratelsky();

};


#endif //MAIN_CPP_NPC_H
