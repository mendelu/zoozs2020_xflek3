//
// Created by david on 22.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_LEKTVAR_H
#define ZOO_PROJECT_REBORN_LEKTVAR_H
#include <iostream>

class Lektvar {
    std::string m_jmeno;
    double m_bonus;
    char m_typ; // 'h'=heal, 's'=strength, 'd'=defence;
public:
    Lektvar(std::string jmeno, double bonus, char typ);
    std::string getJmeno();
    double getBonus();
    char getTyp();
    void printInfo();
};


#endif //ZOO_PROJECT_REBORN_LEKTVAR_H
