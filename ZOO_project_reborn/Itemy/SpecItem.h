//
// Created by david on 22.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_SPECITEM_H
#define ZOO_PROJECT_REBORN_SPECITEM_H
#include <iostream>

class SpecItem {
    std::string m_jmeno;
    std::string m_kod; //pro ověření třeba klíče, že patří k určitý dveřím
public:
    SpecItem(std::string jmeno, std::string kod);
    std::string getJmeno();
    std::string getKod();
};


#endif //ZOO_PROJECT_REBORN_SPECITEM_H
