//
// Created by david on 14.01.2021.
//

#include "Pet.h"
Pet::Pet(std::string jmeno, double sila, double zivoty){
    m_jmeno = jmeno;
    m_sila = sila;
    m_zivoty = zivoty;
}
std::string Pet::getJmeno(){
    return m_jmeno;
}
double Pet::getSila(){
    return m_sila;
}
double Pet::getZivoty(){
    return m_zivoty;
}

double Pet::pridejZivoty(double kolik){
    m_zivoty += kolik;
}