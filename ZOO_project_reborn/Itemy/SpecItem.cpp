//
// Created by david on 22.01.2021.
//

#include "SpecItem.h"
SpecItem::SpecItem(std::string jmeno, std::string kod){
    m_jmeno = jmeno;
    m_kod = kod;
}
std::string SpecItem::getJmeno(){
    return m_jmeno;
}
std::string SpecItem::getKod(){
    return m_kod;
}
