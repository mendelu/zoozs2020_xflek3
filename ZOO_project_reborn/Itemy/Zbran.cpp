//
// Created by david on 14.01.2021.
//

#include "Zbran.h"
Zbran::Zbran(std::string jmeno, double bonusSily){
    m_jmeno = jmeno;
    m_bonusSily = bonusSily;
}
std::string Zbran::getJmeno(){
    return m_jmeno;
}
double Zbran::getBonusSily(){
    return m_bonusSily;
}