//
// Created by david on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_BRNENI_H
#define ZOO_PROJECT_REBORN_BRNENI_H
#include <iostream>

class Brneni {
    std::string m_jmeno;
    double m_bonusObrany;
public:
    Brneni(std::string jmeno, double bonusObrany);
    std::string getJmeno();
    double getBonusObrany();
};


#endif //ZOO_PROJECT_REBORN_BRNENI_H
