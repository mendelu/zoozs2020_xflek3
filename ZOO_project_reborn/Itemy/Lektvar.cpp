//
// Created by david on 22.01.2021.
//

#include "Lektvar.h"
Lektvar::Lektvar(std::string jmeno, double bonus, char typ){
    m_jmeno = jmeno;
    m_bonus = bonus;
    m_typ = typ;
}
std::string Lektvar::getJmeno(){
    return m_jmeno;
}
double Lektvar::getBonus(){
    return m_bonus;
}
char Lektvar::getTyp(){
    return m_typ;
}
void Lektvar::printInfo() {
    std::cout <<  "Jmeno:\t" << getJmeno() << " Typ:\t" << getTyp() << " Bonus:\t" << getBonus() << "\n";
}