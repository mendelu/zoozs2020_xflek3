//
// Created by david on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_PET_H
#define ZOO_PROJECT_REBORN_PET_H
#include<iostream>

class Pet {
    std::string m_jmeno;
    double m_sila;
    double m_zivoty;
public:
    Pet(std::string jmeno, double sila, double zivoty);
    std::string getJmeno();
    double getSila();
    double getZivoty();
    double pridejZivoty(double kolik);
};


#endif //ZOO_PROJECT_REBORN_PET_H
