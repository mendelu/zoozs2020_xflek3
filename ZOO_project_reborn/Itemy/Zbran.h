//
// Created by david on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_ZBRAN_H
#define ZOO_PROJECT_REBORN_ZBRAN_H
#include <iostream>

class Zbran {
    std::string m_jmeno;
    double m_bonusSily;
public:
    Zbran(std::string jmeno, double bonusSily);
    std::string getJmeno();
    double getBonusSily();
};


#endif //ZOO_PROJECT_REBORN_ZBRAN_H
