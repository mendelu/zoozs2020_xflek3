//
// Created by david on 22.01.2021.
//

#include "P_Kaluz.h"
P_Kaluz::P_Kaluz():Prikaz("Kaluz"){}
void P_Kaluz::provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map){
    char anone;
    std::cout << "\nPrisel jsi k zluto-hnede kaluzi. Odporne smrdi a kolem litaji mouchy. Pres povrch se ze dna ovsem neco blysti. Chces do ni skocit a podivat se?\n";
    std::cout <<"(a)no/(n)e: "; std::cin >> anone;
    if(anone != 'a' && anone != 'n'){
        provedPrikaz(hrac, prikazy, mista, map);
    }else if(anone == 'a'){
        std::cout << "\nNa dne zumpovite kaluze jsi nasel novou zbran!\n";
        hrac->setZbran(new Zbran("Kaluzovy mec", 10));
        std::cout << "\nBohuzel sis ale loknul pri vytahovani mece ... -5 zdravi\n";
        hrac->pridatZdravi(-5);
        std::cout << "\nU kaluze na tebe vsak cekal Kentaur. Poradis si s nim?\n" << std::endl;
        hrac->bojuj(hrac->getTovarna()->getKentaur());
    }else{
        std::cout << "Srabe...\n";
    }
}