//
// Created by david on 22.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_KALUZ_H
#define ZOO_PROJECT_REBORN_P_KALUZ_H
#include "../Prikaz.h"

class P_Kaluz:public Prikaz {
public:
    P_Kaluz();
    void provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map);

};


#endif //ZOO_PROJECT_REBORN_P_KALUZ_H
