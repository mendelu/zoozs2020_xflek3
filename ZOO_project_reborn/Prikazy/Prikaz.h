//
// Created by david on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_PRIKAZ_H
#define ZOO_PROJECT_REBORN_PRIKAZ_H
#include <iostream>
#include <vector>
#include "../Player.h"

class Prikaz {
protected:
    std::string m_popis;
public:
    Prikaz(std::string popis);
    virtual void provedPrikaz(Player* hrac, std::vector<Prikaz*> &prikazy, std::vector<std::string> mista, std::string map)=0;
    std::string getPopis();
};


#endif //ZOO_PROJECT_REBORN_PRIKAZ_H
