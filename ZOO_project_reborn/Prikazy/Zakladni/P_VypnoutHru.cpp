//
// Created by david on 21.01.2021.
//

#include "P_VypnoutHru.h"
P_VypnoutHru::P_VypnoutHru():Prikaz("Vypnout hru"){}
void P_VypnoutHru::provedPrikaz(Player* hrac, std::vector<Prikaz*> &prikazy, std::vector<std::string> mista, std::string map){
    std::cout << "Opravdu chcete hru vypnout? [a]no nebo [n]e \n";
    char anone;
    std::cin >> anone;
    if(anone == 'a'){
        std::cout << "Diky za hrani nasi hry!! :)";
        vypnutiHry();
    }else{
        std::cout << "Spravna volba!!";
        return;
    }
}
void P_VypnoutHru::vypnutiHry(){
    exit (0);
}