//
// Created by david on 21.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_VYPNOUTHRU_H
#define ZOO_PROJECT_REBORN_P_VYPNOUTHRU_H
#include "../Prikaz.h"

class P_VypnoutHru:public Prikaz {
public:
    P_VypnoutHru();
    void provedPrikaz(Player* hrac, std::vector<Prikaz*> &prikazy, std::vector<std::string> mista, std::string map);
    void vypnutiHry();
};


#endif //ZOO_PROJECT_REBORN_P_VYPNOUTHRU_H
