//
// Created by david on 20.01.2021.
//

#include "P_RozhledniSe.h"
P_RozhledniSe::P_RozhledniSe():Prikaz("Rozhledni se"){}

void P_RozhledniSe::provedPrikaz(Player* hrac, std::vector<Prikaz*> &prikazy, std::vector<std::string> mista, std::string map){
    vypisMista(mista);
    int vstup = nactiVstupHrace(mista);
    if(vstup == 0){
        return;
    }else{
        prikazy.at(vstup+3)->provedPrikaz(hrac, prikazy, mista, map);
    }

}
void P_RozhledniSe::vypisMista(std::vector<std::string> mista){
    for(int i=0; i<mista.size(); i++){
        std::cout << i+1 << ") " << mista.at(i) << std::endl;
    }
}
int P_RozhledniSe::nactiVstupHrace(std::vector<std::string> mista){
    int vstup;
    std::cout << "Zadej cislo mista (Pro odchod 0): ";
    std::cin >> vstup;
    if(vstup > mista.size() || vstup < 0){
        std::cout << "Zkus to znovu\n";
        nactiVstupHrace(mista);
    }
    return vstup;
}