//
// Created by david on 21.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_OTEVRITMAPU_H
#define ZOO_PROJECT_REBORN_P_OTEVRITMAPU_H
#include "../Prikaz.h"

class P_OtevritMapu:public Prikaz {
public:
    P_OtevritMapu();
    void provedPrikaz(Player* hrac, std::vector<Prikaz*> &prikazy, std::vector<std::string> mista, std::string map);
};


#endif //ZOO_PROJECT_REBORN_P_OTEVRITMAPU_H
