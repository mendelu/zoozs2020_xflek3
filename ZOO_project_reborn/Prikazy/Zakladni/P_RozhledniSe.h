//
// Created by david on 20.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_ROZHLEDNISE_H
#define ZOO_PROJECT_REBORN_P_ROZHLEDNISE_H
#include "../Prikaz.h"

class P_RozhledniSe:public Prikaz {
public:
    P_RozhledniSe();
    void provedPrikaz(Player* hrac, std::vector<Prikaz*> &prikazy, std::vector<std::string> mista, std::string map);
    void vypisMista(std::vector<std::string> mista);
    int nactiVstupHrace(std::vector<std::string> mista);
};


#endif //ZOO_PROJECT_REBORN_P_ROZHLEDNISE_H
