//
// Created by Jan Mecerod on 23.01.2021.
//

#include "P_Studna.h"

P_Studna::P_Studna():Prikaz("Studna") {

}
void P_Studna::provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map){
    char anone;
    std::cout << "\nDosel jsi k opustene studni. Vypada vyschle, ale na dno nedohlednes. Legenda vsak pravi, ze se v ni nachazi nejaky poklad. Chces namotat vedro a podivat se, zda se v nem neco nachazi?\n";
    std::cout <<"(a)no/(n)e: " << std::endl;
    std::cin >> anone;
    if(anone != 'a' && anone != 'n'){
        provedPrikaz(hrac, prikazy, mista, map);
    }else if(anone == 'a'){
        std::cout << "\nNasel jsi 50 zlataku!\n";
        hrac->pridatZlataky(50);
        std::cout << "\nPri vytahovani vedra jsi se trochu vycerpal ... -2 zdravi\n";
        hrac->pridatZdravi(-2);
        std::cout << "\nTvoje radost vsak vzbudila pozornost Kyklopa, ktery se v tomto lese schovava.\n" << std::endl;
        hrac->bojuj(hrac->getTovarna()->getKyklop());
    }else{
        std::cout << "Kdo nic nezkusi, nic nema...\n";
    }
}
