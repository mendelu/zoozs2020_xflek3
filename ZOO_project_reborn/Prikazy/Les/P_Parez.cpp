//
// Created by Jan Mecerod on 23.01.2021.
//

#include "P_Parez.h"

P_Parez::P_Parez():Prikaz("Parez") {

}
void P_Parez::provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map){
    char anone;
    std::cout << "\nStojis u velmi stareho parezu. V dire, kterou rozkousali kurovci vidis nejaky kus kuze. Mohla by se hodit. Chces se podivat zblizka?\n";
    std::cout <<"(a)no/(n)e: " << std::endl;
    std::cin >> anone;
    if(anone != 'a' && anone != 'n'){
        provedPrikaz(hrac, prikazy, mista, map);
    }else if(anone == 'a'){
        std::cout << "\nNasel jsi kozene brneni! Lepsi nez nic, ne?\n";
        Brneni* kozenaVesta = new Brneni("Kozena Vesta", 10);
        hrac->setBrneni(kozenaVesta);
        std::cout << "\nNa parezu jsi si chvilku odpocinul ... +3 zdravi\n";
        hrac->pridatZdravi(3);
    }else{
        std::cout << "Treba tam bylo neco uzitecneho...\n";
    }
}
