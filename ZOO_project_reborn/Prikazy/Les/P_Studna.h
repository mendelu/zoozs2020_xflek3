//
// Created by Jan Mecerod on 23.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_STUDNA_H
#define ZOO_PROJECT_REBORN_P_STUDNA_H
#include "../Prikaz.h"

class P_Studna: public Prikaz {
public:
    P_Studna();
    void provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map);

};


#endif //ZOO_PROJECT_REBORN_P_STUDNA_H
