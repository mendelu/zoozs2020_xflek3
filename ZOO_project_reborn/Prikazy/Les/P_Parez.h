//
// Created by Jan Mecerod on 23.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_PAREZ_H
#define ZOO_PROJECT_REBORN_P_PAREZ_H
#include "../Prikaz.h"

class P_Parez:public Prikaz {
public:
    P_Parez();
    void provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map);

};


#endif //ZOO_PROJECT_REBORN_P_PAREZ_H
