//
// Created by Jan Mecerod on 23.01.2021.
//

#include "P_Trh.h"

P_Trh::P_Trh():Prikaz("Trhovy stanek") {

}

void P_Trh::provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map){
    char anone;
    std::cout << "\nPred tebou stoji stanek s vecmi. Chces se podivat, co se zde nabizi? Muzes si koupit ale jen jednu vec!\n";
    std::cout <<"(a)no/(n)e: " << std::endl;
    std::cin >> anone;
    if(anone != 'a' && anone != 'n'){
        provedPrikaz(hrac, prikazy, mista, map);
    }else if(anone == 'a'){
        std::cout << "\nNabidka: \n";
        std::cout << "1)Kozeny set brneni --- 50 zlataku" << std::endl;
        std::cout << "2)Lektvar zdravi --- 20 zlataku" << std::endl;
        std::cout << "3)Lektvar sily --- 20 zlataku" << std::endl;
        std::cout << "4)Pes --- 40 zlataku" << std::endl;
        std::cout << "\nZadej cislo zbozi, ktere si chces koupit: " << std::endl;
        vyber(hrac);
    }else{
        std::cout << "Stav se priste...\n";
    }
}

int P_Trh::vyber(Player *hrac){
    int vyber;
    std::cin >> vyber;
    if(vyber == 1){
        int cena = 50;
        if(kontrolaPenez(hrac, cena) == true) {
            hrac->pridatZlataky(-cena);
            Brneni *kozenySet = new Brneni("Kozeny set", 25);
            hrac->setBrneni(kozenySet);
        } else {
            std::cout << "Na tohle nemas dostatek penez! Zkus treba chvili rybarit :)" << std::endl;
        }
    } else if(vyber == 2){
        int cena = 20;
        if(kontrolaPenez(hrac, cena) == true) {
            hrac->pridatZlataky(-20);
            Lektvar *healing = new Lektvar("Lektvar zdravi", 25, 'h');
        } else {
            std::cout << "Na tohle nemas dostatek penez!" << std::endl;
        }
    } else if(vyber == 3){
        int cena = 20;
        if(kontrolaPenez(hrac, cena) == true) {
            hrac->pridatZlataky(-20);
            Lektvar *strength = new Lektvar("Lektvar sily", 20, 's');
        } else {
            std::cout << "Na tohle nemas dostatek penez!" << std::endl;
        }
    } else if(vyber == 4){
        int cena = 40;
        if (kontrolaPenez(hrac, cena) == true) {
            hrac->pridatZlataky(-40);
            Pet* pes = new Pet("Alik", 20, 70);
            hrac->setPet(pes);
        } else {
            std::cout << "Na tohle nemas dostatek penez!" << std::endl;
        }
    }
    return vyber;
}

bool P_Trh::kontrolaPenez(Player* hrac, int cena){
    bool dostatek;
    if(hrac->getZlataky() < cena){
        dostatek = false;
    } else if(hrac->getZlataky() >= cena) {
        dostatek = true;
    }
    return dostatek;
}