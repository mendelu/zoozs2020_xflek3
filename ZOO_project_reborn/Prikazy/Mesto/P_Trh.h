//
// Created by Jan Mecerod on 23.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_TRH_H
#define ZOO_PROJECT_REBORN_P_TRH_H
#include "../Prikaz.h"

class P_Trh: public Prikaz {
public:
    P_Trh();
    void provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map);
    int vyber(Player *hrac);
    bool kontrolaPenez(Player* hrac, int cena);
};


#endif //ZOO_PROJECT_REBORN_P_TRH_H
