//
// Created by Jan Mecerod on 23.01.2021.
//

#include "P_Prut.h"

P_Prut::P_Prut():Prikaz("Rybarsky prut") {

}
void P_Prut::provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map){
    char anone;
    std::cout << "\nVidis zde odlozeny rybarsky prut. Chces zkusit sve stesti a chvili chytat ryby?\n";
    std::cout <<"(a)no/(n)e: " << std::endl;
    std::cin >> anone;
    if(anone != 'a' && anone != 'n'){
        provedPrikaz(hrac, prikazy, mista, map);
    }else if(anone == 'a'){
        std::cout << "\nMas stesti! Nasel jsi zlatou rybku! ... Rybka ti vyklouzla z ruky. Aniz by sis neco stihl prat, rybka to udelala za tebe. Pridala ti 10 zivotu a jeste te obdarovala 30 zlataky!\n";
        hrac->pridatZdravi(10);
        hrac->pridatZlataky(30);
        std::cout << "\nJsi tedy stastny smolar.\n";
    }else{
        std::cout << "Vis kolik se toho da ve vode krome stare boty najit?\n";
    }
}