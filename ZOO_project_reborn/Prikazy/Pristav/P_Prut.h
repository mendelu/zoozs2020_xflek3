//
// Created by Jan Mecerod on 23.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_PRUT_H
#define ZOO_PROJECT_REBORN_P_PRUT_H
#include "../Prikaz.h"

class P_Prut: public Prikaz {
public:
    P_Prut();
    void provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map);
};


#endif //ZOO_PROJECT_REBORN_P_PRUT_H
