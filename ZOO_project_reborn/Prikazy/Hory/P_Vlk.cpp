//
// Created by Jan Mecerod on 23.01.2021.
//

#include "P_Vlk.h"

P_Vlk::P_Vlk():Prikaz("Vlk") {

}
void P_Vlk::provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map){
    char anone;
    std::cout << "\nV dalce jsi zahledl vlka. Ten by ti mohl pomoci pri soubojich. Chces si ho zkusit ochocit?\n";
    std::cout <<"(a)no/(n)e: " << std::endl;
    std::cin >> anone;
    if(anone != 'a' && anone != 'n'){
        provedPrikaz(hrac, prikazy, mista, map);
    }else if(anone == 'a'){
        std::cout << "\nPovedlo se. Od ted s tebou bude na cestach chodit vlk!\n";
        Pet* vlk = new Pet("Vlk", 10, 100);
        hrac->setPet(vlk);
        std::cout << "\nOchocovani neprobihalo zcela podle predstav, vlk si ochocoval spise tebe ... -20 zdravi\n";
        hrac->pridatZdravi(-20);
    }else{
        std::cout << "Jsi posera...\n";
    }
}