//
// Created by Jan Mecerod on 23.01.2021.
//

#include "P_Indian.h"

P_Indian::P_Indian():Prikaz("Indian") {

}
void P_Indian::provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map){
    char rozhodnuti;
    std::cout << "\nAjajaj! Dostal jsi se do souboje s indianem. Pokud vyhrajes, muzes ziskat jeho komplet brneni. Chces bojovat nebo zkusit utect?\n";
    std::cout <<"(b)ojovat/(u)tect: " << std::endl;
    std::cin >> rozhodnuti;
    if(rozhodnuti != 'b' && rozhodnuti != 'u'){
        provedPrikaz(hrac, prikazy, mista, map);
    }else if(rozhodnuti == 'b'){
        std::cout << "\nPri boji jsi se moc nenamohl, zatimco jsi dal indianovi jednu ranu, tvoje zvire ho vystrasilo natolik, ze se vzdal. Dostal jsi tak jeho brneni prakticky zadarmo. Stalo te to 'jen' 30 zivotu\n";
        Brneni* indianskyKomplet = new Brneni("Indiansky komplet", 20);
        hrac->setBrneni(indianskyKomplet);
        hrac->pridatZdravi(10);
        hrac->pridatZdravi(-30);
        if(hrac->getZdravi()>20) {
            std::cout << "\nOstep se mu rozpadl, ten si tedy vzit nemuzes.\n";
            std::cout << "\nIndian mel u sebe take lektvar na zivoty ... +10 zdravi\n";
        }
    }else{
        std::cout << "Zakopl jsi a spadl jsi do prikopu ... -7 zdravi\n";
        hrac->pridatZdravi(-7);
        std::cout << "Ma to ale pozitivum, ztratil ses indianovi.\n";
    }
}