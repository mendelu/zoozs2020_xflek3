//
// Created by Jan Mecerod on 23.01.2021.
//

#include "P_Jeskyne.h"

P_Jeskyne::P_Jeskyne():Prikaz("Jeskyne") {

}
void P_Jeskyne::provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map){
    char anone;
    std::cout << "\nVedle tebe vidis nejakou diru do skaly. Vypada to jako jeskyne. Pujdes na prohlidku?\n";
    std::cout <<"(a)no/(n)e: " << std::endl;
    std::cin >> anone;
    if(anone != 'a' && anone != 'n'){
        provedPrikaz(hrac, prikazy, mista, map);
    }else if(anone == 'a'){
        std::cout << "\nZeme se zatrasla a vychod z jeskyne zasypalo kameni. Jeden z balvanu zasahl i tvoje zvire ... -10 zdravi pro tve zvire\n";
        hrac->getPet()->pridejZivoty(-10);
        std::cout << "\nStesti se na tebe ale precejen usmalo, pri vyhrabavani se ven jsi nasel novy ostry mec!\n";
        Zbran* ostryMec = new Zbran("Velmi ostry mec", 30);
        hrac->setZbran(ostryMec);
        std::cout << "\nSesuv kameni v jeskyni slysel i kentaur zijici v horach. Postavis se mu?\n" << std::endl;
        hrac->bojuj(hrac->getTovarna()->getKentaur());
    }else{
        std::cout << "Kdo vi, co jsi mohl najit...\n";
    }
}