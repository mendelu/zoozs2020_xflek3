//
// Created by Jan Mecerod on 23.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_VLK_H
#define ZOO_PROJECT_REBORN_P_VLK_H
#include "../Prikaz.h"

class P_Vlk: public Prikaz {
public:
    P_Vlk();
    void provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map);

};


#endif //ZOO_PROJECT_REBORN_P_VLK_H
