//
// Created by Jan Mecerod on 23.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_P_INDIAN_H
#define ZOO_PROJECT_REBORN_P_INDIAN_H
#include "../Prikaz.h"

class P_Indian: public Prikaz {
public:
    P_Indian();
    void provedPrikaz(Player *hrac, std::vector<Prikaz *> &prikazy, std::vector<std::string> mista, std::string map);
};


#endif //ZOO_PROJECT_REBORN_P_INDIAN_H
