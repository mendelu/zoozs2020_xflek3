//
// Created by david on 20.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_UI_H
#define ZOO_PROJECT_REBORN_UI_H
#include "Player.h"
#include "Lokace/Builder_backend/LokaceDirector.h"
#include "Lokace/SpawnLokaceBuilder.h"
#include "Lokace/MestoLokaceBuilder.h"
#include "Lokace/LesLokaceBuilder.h"
#include "Lokace/PristavLokaceBuilder.h"
#include "Lokace/HoryLokaceBuilder.h"
#include "Prikazy/Zakladni/P_VypnoutHru.h"
#include "Prikazy/Zakladni/P_RozhledniSe.h"
#include "Prikazy/Zakladni/P_OtevritMapu.h"
#include "Prikazy/Zakladni/P_ZkontrolovatStav.h"
class Ui {
    std::vector<Prikaz*> m_prikazy;
    Lokace* m_aktualniLokace;
    Player* m_hrac;
public:
    Ui();
    void vypisPrikazy();
    void coTed();
    int nactiVstupHrace();
    void provedPrikaz(int ktery);
    void zacatekHry();
    int nacteniObtiznosti();
    void nactiZakladniPrikazy(); //Takové ty, které jsou všude
    void smazExtraPrikazy();
    void nactiExtraPrikazy(); //Z lokací
    void vypisSousedy();

};


#endif //ZOO_PROJECT_REBORN_UI_H
