//
// Created by david on 22.01.2021.
//

#include "LesLokaceBuilder.h"
LesLokaceBuilder::LesLokaceBuilder(){}

void LesLokaceBuilder::buildLokace(){
    m_lokace->setJmeno("Les");
    m_lokace->setMapa("/--------------------------------------------------------------------------------------------------------------------\\\n"
                      "|                                                                                                                    |\n"
                      "|                                                                              ^                                     |\n"
                      "|                                                                ^        ^   / \\                                    |\n"
                      "|                                                            ^  / \\  /---/ \\--   \\                                   |\n"
                      "|                                                        ^  / \\/   ----    /      \\---\\                              |\n"
                      "|     _________                                         / \\/   \\  /    \\  /            \\                             |\n"
                      "|    /*********\\                                       /   \\     /      \\               \\                            |\n"
                      "|   /***********\\                                     /     \\    | HORY |---                                         |\n"
                      "|  /*************\\                                               |      |###\\--                                      |\n"
                      "| /|             |\\                                              \\      /-\\####\\--                                   |\n"
                      "|  |    SPAWN    |                                                \\    /   --\\####\\--                                |\n"
                      "|  |             |                                                 ----       --\\####\\--------------                 |\n"
                      "|  |             |                                                               --\\################\\                |\n"
                      "|  ---------------                                                                  --------------\\##\\--   /\\        |\n"
                      "|        |#|                                                                                       \\####\\ /||\\       |\n"
                      "|        |#|                                                                                    /\\  --\\#| /||\\       |\n"
                      "|         \\#\\                                                                                  /||\\   |#|  ||        |\n"
                      "|          \\#\\--------------                                                                   /||\\  -----    /\\     |\n"
                      "|           \\###############\\---                                                                ||  /     \\  /||\\    |\n"
                      "|            --------------\\####\\             -------                                ----->        /       \\ /||\\    |\n"
                      "|                           ---\\#\\-          /       \\                                          ---|  LES  |  ||     |\n"
                      "|                               \\##\\-       /         \\                                      --/###|       |         |\n"
                      "|                                -\\##\\---  /           \\                                  --/####/-\\       /         |\n"
                      "|                                  -\\####\\-|           |--------                       --/####/--   \\     /          |\n"
                      "|                                    ---\\##|  NAMESTI  |########\\----              ---/####/--       -----   /\\      |\n"
                      "|                                        --|           |------\\######\\------------/####/---                 /||\\     |\n"
                      "|                                          \\           /       ----\\################/--           /\\    /\\  /||\\     |\n"
                      "|                                           \\         /             ----------------             /||\\  /||\\  ||      |\n"
                      "|                                            \\       /                                           /||\\  /||\\          |\n"
                      "|                                             -------                                             ||    ||           |\n"
                      "|                                               |#|                                                                  |\n"
                      "|                                               |#|                                                              ____|\n"
                      "|                                               |#\\-                                                          __/    |\n"
                      "|                                                \\##\\-                                                   ____/       |\n"
                      "|                                                 \\###\\---         ------                           ____/            |\n"
                      "|                                                  -\\#####\\-------/######\\---                ______/                 |\n"
                      "|                                                    ---\\###########/--\\#####\\--            /              ~~~~~     |\n"
                      "|                                                        -----------    ---\\####\\-        --              ~~   ~~~~~ |\n"
                      "|                                                                           --\\###\\-    _/    PRISTAV   ~~~          |\n"
                      "|                                                                              --\\##\\- /                             |\n"
                      "|                                                                                 \\###/                              |\n"
                      "|                                                                                  --/                   ~~~~~~~~~   |\n"
                      "|                                                                                 _/      ---       ~~~~~~~~~        |\n"
                      "|                                                                               _/       /*  \\/|   ~~                |\n"
                      "|                                                                              /         &   /\\|                     |\n"
                      "|                                                                             /          \\---              ~~~~      |\n"
                      "|                                                                            /                          ~~~~         |\n"
                      "|                                                                                                                    |\n"
                      "\\--------------------------------------------------------------------------------------------------------------------/");
    postavSousedy();
    m_lokace->pridejPrikazy(createPrikazy());
    setMista();
}

std::vector<Prikaz*> LesLokaceBuilder::createPrikazy(){
    std::vector<Prikaz*> prikazy;
    prikazy.push_back(new P_Parez());
    prikazy.push_back(new P_Studna());
    return prikazy;
}

void LesLokaceBuilder::setMista(){
    for(int i=0; i<m_lokace->getPrikazy().size(); i++){
        m_lokace->pridejMisto(m_lokace->getPrikazy().at(i)->getPopis());
    }
}

//nejsem si uplne jistej, jestli to mam spravne
void LesLokaceBuilder::postavSousedy(){
    LokaceDirector* sefik2 = new LokaceDirector(new HoryLokaceBuilder);
    m_lokace ->pridejSouseda(sefik2->constructLokace());
    m_lokace->getSoused(0)->pridejCestuZpet(m_lokace);
}