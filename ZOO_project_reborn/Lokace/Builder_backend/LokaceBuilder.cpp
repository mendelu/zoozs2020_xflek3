//
// Created by Jan Mecerod on 14.01.2021.
//

#include "LokaceBuilder.h"

LokaceBuilder::LokaceBuilder(){
    m_lokace = new Lokace();
}

void LokaceBuilder::createNewLokace(){
    m_lokace = new Lokace();
}


Lokace* LokaceBuilder::getLokace(){
    return m_lokace;
}