//
// Created by Jan Mecerod on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_LOKACEBUILDER_H
#define ZOO_PROJECT_REBORN_LOKACEBUILDER_H
#include <iostream>
#include "Lokace.h"

class LokaceBuilder {
protected:
    Lokace* m_lokace;
public:
    LokaceBuilder();
    void createNewLokace();
    virtual void buildLokace()=0;
    virtual std::vector<Prikaz*> createPrikazy()=0;
    Lokace* getLokace();
};


#endif //ZOO_PROJECT_REBORN_LOKACEBUILDER_H
