//
// Created by Jan Mecerod on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_LOKACEDIRECTOR_H
#define ZOO_PROJECT_REBORN_LOKACEDIRECTOR_H
#include "LokaceBuilder.h"
#include "../../Prikazy/Prikaz.h"
class LokaceDirector {
protected:
    LokaceBuilder* m_builder;
public:
    LokaceDirector(LokaceBuilder* builder);
    void setLokaceBuilder(LokaceBuilder* builder);
    Lokace* constructLokace();
};


#endif //ZOO_PROJECT_REBORN_LOKACEDIRECTOR_H
