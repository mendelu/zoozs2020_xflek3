//
// Created by Jan Mecerod on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_LOKACE_H
#define ZOO_PROJECT_REBORN_LOKACE_H
#include <iostream>
#include <vector>
#include "../../Prikazy/Prikaz.h"

class Lokace {
    std::string m_jmeno;
    std::string m_mapa;
    std::vector <std::string> m_mista;
    std::vector <Lokace*> m_sousedi;
    std::vector <Prikaz*> m_prikazy;
public:
    Lokace();
    void pridejSouseda(Lokace* soused);
    void pridejCestuZpet(Lokace* cesta);
    void setJmeno(std::string jmeno);
    std::string getJmeno();
    void setMapa(std::string mapa);
    Lokace* getSoused(int ktery);
    std::vector <Lokace*> getSousedy();
    void pridejPrikazy(std::vector<Prikaz*> prikazy);
    void pridejMisto(std::string misto);
    std::vector<Prikaz*> getPrikazy();
    std::vector <std::string> getMista();
    std::string getMapa();
};


#endif //ZOO_PROJECT_REBORN_LOKACE_H
