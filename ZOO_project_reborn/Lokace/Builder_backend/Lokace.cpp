//
// Created by Jan Mecerod on 14.01.2021.
//

#include "Lokace.h"

Lokace::Lokace() {
    m_jmeno = "";
    m_mapa = "";

}
void Lokace::pridejSouseda(Lokace* soused){
    m_sousedi.push_back(soused);
}
void Lokace::pridejCestuZpet(Lokace* cesta){
    m_sousedi.push_back(cesta);
}
Lokace* Lokace::getSoused(int ktery){
    if(ktery < m_sousedi.size() && ktery > -1){
        return m_sousedi.at(ktery);
    }else return m_sousedi.at(0); // blbost, ale prozatím...

}
std::vector <Lokace*> Lokace::getSousedy(){
    return m_sousedi;
}
std::string Lokace::getJmeno(){
    return m_jmeno;
}
void Lokace::setJmeno(std::string jmeno){
    m_jmeno = jmeno;
}
void Lokace::setMapa(std::string mapa){
    m_mapa = mapa;
}
void Lokace::pridejPrikazy(std::vector<Prikaz*> prikazy){
    m_prikazy = prikazy;
}
void Lokace::pridejMisto(std::string misto){
    m_mista.push_back(misto);
}
std::vector<Prikaz*> Lokace::getPrikazy(){
    return m_prikazy;
}
std::vector <std::string> Lokace::getMista(){
    return m_mista;
}
std::string Lokace::getMapa(){
    return m_mapa;
}