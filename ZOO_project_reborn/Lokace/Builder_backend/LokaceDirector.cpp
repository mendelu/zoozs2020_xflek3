//
// Created by Jan Mecerod on 14.01.2021.
//

#include "LokaceDirector.h"

LokaceDirector::LokaceDirector(LokaceBuilder* builder){
    m_builder = builder;
}

void LokaceDirector::setLokaceBuilder(LokaceBuilder* builder){
    m_builder = builder;
}

Lokace* LokaceDirector::constructLokace(){
    m_builder->buildLokace();
    return m_builder->getLokace();
}