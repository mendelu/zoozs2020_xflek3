//
// Created by Jan Mecerod on 14.01.2021.
//

#include "MestoLokaceBuilder.h"

MestoLokaceBuilder::MestoLokaceBuilder(){
}

void MestoLokaceBuilder::buildLokace(){
    m_lokace->setJmeno("Mesto");
    m_lokace->setMapa("/--------------------------------------------------------------------------------------------------------------------\\\n"
                      "|                                                                                                                    |\n"
                      "|                                                                              ^                                     |\n"
                      "|                                                                ^        ^   / \\                                    |\n"
                      "|                                                            ^  / \\  /---/ \\--   \\                                   |\n"
                      "|                                                        ^  / \\/   ----    /      \\---\\                              |\n"
                      "|     _________                                         / \\/   \\  /    \\  /            \\                             |\n"
                      "|    /*********\\                                       /   \\     /      \\               \\                            |\n"
                      "|   /***********\\                                     /     \\    | HORY |---                                         |\n"
                      "|  /*************\\                                               |      |###\\--                                      |\n"
                      "| /|             |\\                                              \\      /-\\####\\--                                   |\n"
                      "|  |    SPAWN    |                                                \\    /   --\\####\\--                                |\n"
                      "|  |             |                                                 ----       --\\####\\--------------                 |\n"
                      "|  |             |                                                               --\\################\\                |\n"
                      "|  ---------------                                                                  --------------\\##\\--   /\\        |\n"
                      "|        |#|                                                                                       \\####\\ /||\\       |\n"
                      "|        |#|                                     |                                              /\\  --\\#| /||\\       |\n"
                      "|         \\#\\                                    |                                             /||\\   |#|  ||        |\n"
                      "|          \\#\\--------------                    \\|/                                            /||\\  -----    /\\     |\n"
                      "|           \\###############\\---                 v                                              ||  /     \\  /||\\    |\n"
                      "|            --------------\\####\\             -------                                              /       \\ /||\\    |\n"
                      "|                           ---\\#\\-          /       \\                                          ---|  LES  |  ||     |\n"
                      "|                               \\##\\-       /         \\                                      --/###|       |         |\n"
                      "|                                -\\##\\---  /           \\                                  --/####/-\\       /         |\n"
                      "|                                  -\\####\\-|           |--------                       --/####/--   \\     /          |\n"
                      "|                                    ---\\##|  NAMESTI  |########\\----              ---/####/--       -----   /\\      |\n"
                      "|                                        --|           |------\\######\\------------/####/---                 /||\\     |\n"
                      "|                                          \\           /       ----\\################/--           /\\    /\\  /||\\     |\n"
                      "|                                           \\         /             ----------------             /||\\  /||\\  ||      |\n"
                      "|                                            \\       /                                           /||\\  /||\\          |\n"
                      "|                                             -------                                             ||    ||           |\n"
                      "|                                               |#|                                                                  |\n"
                      "|                                               |#|                                                              ____|\n"
                      "|                                               |#\\-                                                          __/    |\n"
                      "|                                                \\##\\-                                                   ____/       |\n"
                      "|                                                 \\###\\---         ------                           ____/            |\n"
                      "|                                                  -\\#####\\-------/######\\---                ______/                 |\n"
                      "|                                                    ---\\###########/--\\#####\\--            /              ~~~~~     |\n"
                      "|                                                        -----------    ---\\####\\-        --              ~~   ~~~~~ |\n"
                      "|                                                                           --\\###\\-    _/    PRISTAV   ~~~          |\n"
                      "|                                                                              --\\##\\- /                             |\n"
                      "|                                                                                 \\###/                              |\n"
                      "|                                                                                  --/                   ~~~~~~~~~   |\n"
                      "|                                                                                 _/      ---       ~~~~~~~~~        |\n"
                      "|                                                                               _/       /*  \\/|   ~~                |\n"
                      "|                                                                              /         &   /\\|                     |\n"
                      "|                                                                             /          \\---              ~~~~      |\n"
                      "|                                                                            /                          ~~~~         |\n"
                      "|                                                                                                                    |\n"
                      "\\--------------------------------------------------------------------------------------------------------------------/");
    postavSousedy();
    m_lokace->pridejPrikazy(createPrikazy());
}
std::vector<Prikaz*> MestoLokaceBuilder::createPrikazy(){
    std::vector<Prikaz*> prikazy;
    prikazy.push_back(new P_Trh());
    return prikazy;
}

void MestoLokaceBuilder::setMista(){
    for(int i=0; i<m_lokace->getPrikazy().size(); i++){
        m_lokace->pridejMisto(m_lokace->getPrikazy().at(i)->getPopis());
    }
}

void MestoLokaceBuilder::postavSousedy(){
    LokaceDirector* sefik1 = new LokaceDirector(new LesLokaceBuilder());
    m_lokace ->pridejSouseda(sefik1->constructLokace());
    m_lokace->getSoused(0)->pridejCestuZpet(m_lokace);
    sefik1->setLokaceBuilder(new PristavLokaceBuilder());
    m_lokace ->pridejSouseda(sefik1->constructLokace());
    m_lokace ->getSoused(1)->pridejCestuZpet(m_lokace);

}
