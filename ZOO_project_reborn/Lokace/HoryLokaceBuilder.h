//
// Created by Jan Mecerod on 23.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_HORYLOKACEBUILDER_H
#define ZOO_PROJECT_REBORN_HORYLOKACEBUILDER_H
#include <iostream>
#include "Builder_backend/LokaceBuilder.h"
#include "Builder_backend/LokaceDirector.h"
#include "SpawnLokaceBuilder.h"
#include "../Prikazy/Hory/P_Jeskyne.h"
#include "../Prikazy/Hory/P_Vlk.h"
#include "../Prikazy/Hory/P_Indian.h"


class HoryLokaceBuilder:public LokaceBuilder {
public:
    HoryLokaceBuilder();
    void buildLokace();
    std::vector<Prikaz*> createPrikazy();
    void setMista();
    void postavSousedy();

};


#endif //ZOO_PROJECT_REBORN_HORYLOKACEBUILDER_H
