//
// Created by Jan Mecerod on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_MESTOLOKACEBUILDER_H
#define ZOO_PROJECT_REBORN_MESTOLOKACEBUILDER_H
#include <iostream>
#include "Builder_backend/LokaceDirector.h"
#include "LesLokaceBuilder.h"
#include "SpawnLokaceBuilder.h"
#include "HoryLokaceBuilder.h"
#include "PristavLokaceBuilder.h"
#include "../Prikazy/Mesto/P_Trh.h"

class MestoLokaceBuilder: public LokaceBuilder {
public:
    MestoLokaceBuilder();
    void buildLokace();
    std::vector<Prikaz*> createPrikazy();
    void postavSousedy();
    void setMista();
};


#endif //ZOO_PROJECT_REBORN_MESTOLOKACEBUILDER_H
