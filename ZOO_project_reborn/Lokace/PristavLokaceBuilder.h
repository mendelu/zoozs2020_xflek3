//
// Created by Jan Mecerod on 23.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_PRISTAVLOKACEBUILDER_H
#define ZOO_PROJECT_REBORN_PRISTAVLOKACEBUILDER_H
#include <iostream>
#include "Builder_backend/LokaceBuilder.h"
#include "Builder_backend/LokaceDirector.h"
#include "../Prikazy/Pristav/P_Prut.h"

class PristavLokaceBuilder:public LokaceBuilder {
public:
    PristavLokaceBuilder();
    void buildLokace();
    std::vector<Prikaz*> createPrikazy();
    void setMista();
    void postavSousedy();
};


#endif //ZOO_PROJECT_REBORN_PRISTAVLOKACEBUILDER_H
