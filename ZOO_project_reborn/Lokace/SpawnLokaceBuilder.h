//
// Created by Jan Mecerod on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_SPAWNLOKACEBUILDER_H
#define ZOO_PROJECT_REBORN_SPAWNLOKACEBUILDER_H
#include <iostream>
#include "Builder_backend/LokaceBuilder.h"
#include "Builder_backend/LokaceDirector.h"
#include "../Prikazy/Spawn/P_Kaluz.h"
#include "MestoLokaceBuilder.h"

class SpawnLokaceBuilder:public LokaceBuilder {
public:
    SpawnLokaceBuilder();
    void buildLokace();
    std::vector<Prikaz*> createPrikazy();
    void setMista();
    void postavSousedy();

};


#endif //ZOO_PROJECT_REBORN_SPAWNLOKACEBUILDER_H
