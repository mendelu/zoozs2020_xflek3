//
// Created by david on 22.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_LESLOKACEBUILDER_H
#define ZOO_PROJECT_REBORN_LESLOKACEBUILDER_H
#include "Builder_backend/LokaceDirector.h"
#include "../Prikazy/Les/P_Parez.h"
#include "../Prikazy/Les/P_Studna.h"
#include "MestoLokaceBuilder.h"
#include "SpawnLokaceBuilder.h"
#include "HoryLokaceBuilder.h"

class LesLokaceBuilder:public LokaceBuilder {
public:
    LesLokaceBuilder();
    void buildLokace();
    std::vector<Prikaz*> createPrikazy();
    void postavSousedy();
    void setMista();
};


#endif //ZOO_PROJECT_REBORN_LESLOKACEBUILDER_H
