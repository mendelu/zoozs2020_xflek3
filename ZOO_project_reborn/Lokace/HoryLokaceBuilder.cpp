//
// Created by Jan Mecerod on 23.01.2021.
//

#include "HoryLokaceBuilder.h"

HoryLokaceBuilder::HoryLokaceBuilder() {
}

void HoryLokaceBuilder::buildLokace(){
    m_lokace->setJmeno("Hory");
    m_lokace->setMapa("/--------------------------------------------------------------------------------------------------------------------\\\n"
                      "|                                                                                                                    |\n"
                      "|                                                                              ^                                     |\n"
                      "|                                                                ^        ^   / \\                                    |\n"
                      "|                                                            ^  / \\  /---/ \\--   \\                                   |\n"
                      "|                                                        ^  / \\/   ----    /      \\---\\                              |\n"
                      "|     _________                                         / \\/   \\  /    \\  /            \\                             |\n"
                      "|    /*********\\                                       /   \\     /      \\               \\                            |\n"
                      "|   /***********\\                                     /     \\    | HORY |---                                         |\n"
                      "|  /*************\\                                               |      |###\\--                                      |\n"
                      "| /|             |\\                                              \\      /-\\####\\--                                   |\n"
                      "|  |    SPAWN    |                                                \\    /   --\\####\\--                                |\n"
                      "|  |             |                                                 ----       --\\####\\--------------                 |\n"
                      "|  |             |                                                               --\\################\\                |\n"
                      "|  ---------------                                                   ^              --------------\\##\\--   /\\        |\n"
                      "|        |#|                                                        /|\\                            \\####\\ /||\\       |\n"
                      "|        |#|                                                         |                          /\\  --\\#| /||\\       |\n"
                      "|         \\#\\                                                        |                         /||\\   |#|  ||        |\n"
                      "|          \\#\\--------------                                                                   /||\\  -----    /\\     |\n"
                      "|           \\###############\\---                                                                ||  /     \\  /||\\    |\n"
                      "|            --------------\\####\\             -------                                              /       \\ /||\\    |\n"
                      "|                           ---\\#\\-          /       \\                                          ---|  LES  |  ||     |\n"
                      "|                               \\##\\-       /         \\                                      --/###|       |         |\n"
                      "|                                -\\##\\---  /           \\                                  --/####/-\\       /         |\n"
                      "|                                  -\\####\\-|           |--------                       --/####/--   \\     /          |\n"
                      "|                                    ---\\##|  NAMESTI  |########\\----              ---/####/--       -----   /\\      |\n"
                      "|                                        --|           |------\\######\\------------/####/---                 /||\\     |\n"
                      "|                                          \\           /       ----\\################/--           /\\    /\\  /||\\     |\n"
                      "|                                           \\         /             ----------------             /||\\  /||\\  ||      |\n"
                      "|                                            \\       /                                           /||\\  /||\\          |\n"
                      "|                                             -------                                             ||    ||           |\n"
                      "|                                               |#|                                                                  |\n"
                      "|                                               |#|                                                              ____|\n"
                      "|                                               |#\\-                                                          __/    |\n"
                      "|                                                \\##\\-                                                   ____/       |\n"
                      "|                                                 \\###\\---         ------                           ____/            |\n"
                      "|                                                  -\\#####\\-------/######\\---                ______/                 |\n"
                      "|                                                    ---\\###########/--\\#####\\--            /              ~~~~~     |\n"
                      "|                                                        -----------    ---\\####\\-        --              ~~   ~~~~~ |\n"
                      "|                                                                           --\\###\\-    _/    PRISTAV   ~~~          |\n"
                      "|                                                                              --\\##\\- /                             |\n"
                      "|                                                                                 \\###/                              |\n"
                      "|                                                                                  --/                   ~~~~~~~~~   |\n"
                      "|                                                                                 _/      ---       ~~~~~~~~~        |\n"
                      "|                                                                               _/       /*  \\/|   ~~                |\n"
                      "|                                                                              /         &   /\\|                     |\n"
                      "|                                                                             /          \\---              ~~~~      |\n"
                      "|                                                                            /                          ~~~~         |\n"
                      "|                                                                                                                    |\n"
                      "\\--------------------------------------------------------------------------------------------------------------------/");
    postavSousedy();
    m_lokace->pridejPrikazy(createPrikazy());
    setMista();
}
std::vector<Prikaz*> HoryLokaceBuilder::createPrikazy(){
    std::vector<Prikaz*> prikazy;
    prikazy.push_back(new (P_Indian));
    prikazy.push_back(new (P_Jeskyne));
    prikazy.push_back(new (P_Vlk));
    return prikazy;
}
void HoryLokaceBuilder::setMista(){
    for(int i=0; i<m_lokace->getPrikazy().size(); i++){
        m_lokace->pridejMisto(m_lokace->getPrikazy().at(i)->getPopis());
    }
}
void HoryLokaceBuilder::postavSousedy(){

}