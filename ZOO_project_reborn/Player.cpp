//
// Created by david on 14.01.2021.
//

#include "Player.h"
Player::Player(std::string jmeno){
    m_jmeno = jmeno;
    m_sila=0;
    m_obrana=0;
    m_zdravi=100;
    m_zlataky=0;
    m_obtiznost=0;
    m_tovarna = new TovarnaStrednichMonster;
    m_pet = new Pet("Kruta", 1, 50);
    m_brneni = new Brneni("Tricko", 5);
    m_zbran = new Zbran("Klacek", 5);
    m_obrana += m_brneni->getBonusObrany();
    m_sila += m_zbran->getBonusSily();
    m_inventar = new Inventory();
    pridatLektvar('h');
}

std::string Player::getJmeno(){
    return m_jmeno;
}
double Player::getZdravi(){
    return m_zdravi;
}
double Player::getSila(){
    return m_sila;
}
double Player::getObrana(){
    return m_obrana;
}
double Player::getZlataky(){
    return m_zlataky;
}
Pet* Player::getPet(){
    return m_pet;
}
Brneni* Player::getBrneni(){
    return m_brneni;
}
Zbran* Player::getZbran(){
    return m_zbran;
}
int Player::getObtiznost(){
    return m_obtiznost;
}
void Player::setJmeno(std::string jmeno){
    m_jmeno = jmeno;
}
void Player::setObtiznost(int obtiznost){
    m_obtiznost = obtiznost;
    if(obtiznost == 2){
        m_tovarna = new TovarnaTezkychMonster;
    }
}
TovarnaMonster* Player::getTovarna(){
    return m_tovarna;
}

void Player::pridatZdravi(double kolik){
    m_zdravi += kolik;
    zkontrolujZdravi();
}
void Player::zkontrolujZdravi(){
    if(m_zdravi >= 100) {
        m_zdravi = 100;
    } else if (m_zdravi <= 0) {
        m_zdravi = 0;
        std::cout << "\n ZEMREL JSI. HRA ZDE KONCI A VYPNE SE" << std::endl;
        exit (0);
    }
    if(m_pet->getZivoty()<=0){
        std::cout << "Zemrelo ti zvire! :(" << std::endl;
        delete m_pet;
    }
}
void Player::pridatSilu(double kolik){
    m_sila += kolik;
}
void Player::pridatObranu(double kolik){
    m_obrana += kolik;
}
void Player::pridatZlataky(double kolik){
    m_zlataky += kolik;
}
void Player::setPet(Pet* pet){
    delete m_pet;
    m_pet = pet;
}
void Player::setBrneni(Brneni* brneni){
    double puvodniBonus = m_brneni->getBonusObrany();
    pridatObranu(-m_brneni->getBonusObrany());
    delete m_brneni;
    m_brneni = brneni;
    pridatObranu(m_brneni->getBonusObrany());
    std::cout << "Zvysila se ti obrana o " << m_brneni->getBonusObrany()-puvodniBonus << std::endl;
}
void Player::setZbran(Zbran* zbran){
    double puvodniBonus = m_zbran->getBonusSily();
    pridatSilu(-m_zbran->getBonusSily());
    delete m_zbran;
    m_zbran = zbran;
    pridatSilu(m_zbran->getBonusSily());
    std::cout << "Zvysila se ti sila o " << m_zbran->getBonusSily()-puvodniBonus << std::endl;
}
void Player::printInfo(){
    std::cout << "\nJmeno:\t" << getJmeno() << std::endl;
    std::cout << "Zdravi:\t" << getZdravi() << std::endl;
    std::cout << "Sila:\t" << getSila() << std::endl;
    std::cout << "Obrana:\t" << getObrana() << std::endl;
    std::cout << "Zlato:\t" << getZlataky() << std::endl;
    std::cout << "Pet:\t" << getPet()->getJmeno() << std::endl;
    std::cout << "Zbran:\t" << getZbran()->getJmeno() << std::endl;
    std::cout << "Brneni:\t" << getBrneni()->getJmeno() << std::endl;
    std::cout << "Obtiznost:\t" << getObtiznost() << std::endl;
    std::cout << "\nChces zkontrolovat svuj inventar? (a)no/(n)e:";
    char anone;
    std::cin >> anone;
    if(anone == 'a'){
        checkInventory();
    }
}

void Player::checkInventory(){
    m_inventar->printObsah();
    //TODO: nějake použití věcí atak
}

void Player::vypijLektvar(Lektvar* lektvar) {

    std::cout << "Pouzil jsi: " << lektvar->getJmeno() << std::endl;
    std::cout << "Doplnil ti: " << lektvar->getBonus();
    vypisBonusLektvaru(lektvar);
    std::cout << std::endl;

    if(lektvar->getTyp()=='h'){
        m_zdravi+= lektvar->getBonus();
    } else if(lektvar->getTyp()=='s'){
        m_sila+= lektvar->getBonus();
    } else if(lektvar->getTyp()=='d'){
        m_obrana+= lektvar->getBonus();
    }
}

void Player::vypisBonusLektvaru(Lektvar* lektvar){
    if(lektvar->getTyp()=='h'){
        std::cout << " zivotu" << std::endl;
    } else if(lektvar->getTyp()=='s'){
        std::cout << " sily" << std::endl;
    } else if(lektvar->getTyp()=='d'){
        std::cout << " bodu obrany" << std:: endl;
    }
}

void Player::pridatLektvar(char n){
    m_inventar->pridatLektvar(n);
}

void Player::bojuj(Nepritel* nepritel){
    bool skoncit = false;
    std::cout << "\nZacinas souboj s " << nepritel->getJmeno() << std::endl;
    while(skoncit == false){
        etapaBoje(nepritel, skoncit);
        if(skoncit == false){
            printInfoOSouboji(nepritel, skoncit);
        }
    }
    std::cout << "\nSkoncil souboj" << std::endl;
    std::cout << "\nMas " << getZdravi() << " zivotu." << std::endl;

}

void Player::printInfoOSouboji(Nepritel* nepritel, bool &skoncit){
    std::cout << "Nepritelovy zivoty: " << nepritel->getZivoty() << std::endl;
    std::cout << "Tvoje zivoty: " << getZdravi() << std::endl;
    std::cout << "Chces utect nebo pouzit lektvar? (u)tect/(l)ektvar:" << std::endl;
    m_inventar->printLektvary();
    char anone;
    std::cin >> anone;
    if (anone == 'l'){
        std::cout << "Zadej pozici: ";
        int cislo;
        std::cin >> cislo;
        vypijLektvar(m_inventar->getLektvar(cislo));
        m_inventar->odhoditLektvar(cislo);
    } else if(anone == 'u'){
        skoncit = true;
    }
}

void Player::etapaBoje(Nepritel* &nepritel, bool &skoncit){
    nepritel->setZivoty(-getSila());
    pridatZdravi(-nepritel->getSilaUtoku()+(m_obrana/100)*nepritel->getSilaUtoku());
    if(nepritel->getZivoty() <= 0){
        skoncit = true;
    }
}


