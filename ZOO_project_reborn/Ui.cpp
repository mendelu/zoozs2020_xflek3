//
// Created by david on 20.01.2021.
//

#include "Ui.h"
Ui::Ui(){
    LokaceDirector* director = new LokaceDirector(new SpawnLokaceBuilder());
    m_aktualniLokace = director->constructLokace();
    nactiZakladniPrikazy();
    nactiExtraPrikazy();
    m_hrac = new Player("NoName");
}
void Ui::vypisPrikazy(){
    for(int i=0; i<4; i++){
        std::cout << i << ") " << m_prikazy.at(i)->getPopis() << std::endl;
    }
}
int Ui::nactiVstupHrace(){
    int vstup;
    std::cout << "Napis cislo: ";
    std::cin >> vstup;
    while(vstup != -1 && vstup!= 0 && vstup!= 1 && vstup!= 2 && vstup !=3){
        std::cout << "Asi jsi se nekde upsal. Zadane cislo musi byt jedno z techto: " << std::endl;
        vypisPrikazy();
        std::cin >> vstup;
    }
    return vstup;
}
void Ui::provedPrikaz(int ktery){
    m_prikazy.at(ktery)->provedPrikaz(m_hrac, m_prikazy, m_aktualniLokace->getMista(), m_aktualniLokace->getMapa());
}
void Ui::coTed(){
    std::cout << "\nCo ted sefe?\n";
    std::cout << "-1) Presun do jine lokace" << std::endl;
    vypisPrikazy();
    int vstup = nactiVstupHrace();
    if(vstup == -1){
        vypisSousedy();
        int vstup1 = nactiVstupHrace();
        std::cout << "Presouvas se do " << m_aktualniLokace->getSoused(vstup1)->getJmeno() << std::endl;
        m_aktualniLokace = m_aktualniLokace->getSoused(vstup1);
        smazExtraPrikazy();
        nactiExtraPrikazy();
    }else {
        provedPrikaz(vstup);
    }
    coTed();
}
void Ui::vypisSousedy(){
    for(int i=0; i<m_aktualniLokace->getSousedy().size(); i++){
        std::cout << i << ") " << m_aktualniLokace->getSoused(i)->getJmeno() << std::endl;
    }
}
void Ui::zacatekHry(){
    std::cout << "Vitej v Jumanji! Nejdrive si zvol jmeno vyvolavaci!" << std::endl;
    std::string jmeno;
    getline(std::cin, jmeno);
    m_hrac->setJmeno(jmeno);
    std::cout << "Cool jmeno." << std::endl;
    m_hrac->setObtiznost(nacteniObtiznosti());
    coTed();
}

int Ui::nacteniObtiznosti() {
    std::cout << "Ted si zvol obtiznost!" << std::endl;
    std::cout << "1) = stredni, 2) = tezka" << std::endl;
    int obtiznost = 0;
    std::cin >> obtiznost;
    while(obtiznost != 1 && obtiznost != 2){
        std::cout << "1) = stredni, 2) = tezka" << std::endl;
        std::cin >> obtiznost;
    }
    return obtiznost;
}

void Ui::nactiZakladniPrikazy(){
    m_prikazy.push_back(new P_VypnoutHru);
    m_prikazy.push_back(new P_RozhledniSe);
    m_prikazy.push_back(new P_ZkontrolovatStav);
    m_prikazy.push_back(new P_OtevritMapu);
}
void Ui::smazExtraPrikazy(){
    while(m_prikazy.size()>4){
        m_prikazy.pop_back();
    }
}
void Ui::nactiExtraPrikazy(){
    std::vector<Prikaz*> P = m_aktualniLokace->getPrikazy();
    for(int i=0; i<P.size(); i++){
        m_prikazy.push_back(P.at(i));
    }
}