//
// Created by david on 22.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_INVENTORY_H
#define ZOO_PROJECT_REBORN_INVENTORY_H
#include <array>
#include <vector>
#include <iostream>
#include "Itemy/Lektvar.h"
#include "Itemy/SpecItem.h"

class Inventory {
    std::array<Lektvar*, 4> m_lektvary;
    std::vector<SpecItem*> m_specItemy;
public:
    Inventory();
    void printObsah();
    void printLektvary();
    void printSpecItemy();
    void pridatLektvar(char n);
    void odhoditLektvar(int ktery);
    void pridatSpecItem(SpecItem* predmet);
    void pouzitSpecItem(int ktery);
    void serazeniSpecItemu();
    Lektvar* getLektvar(int ktery);
};


#endif //ZOO_PROJECT_REBORN_INVENTORY_H
