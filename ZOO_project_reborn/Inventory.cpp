//
// Created by david on 22.01.2021.
//

#include "Inventory.h"
Lektvar* zadnyLektvar = new Lektvar("Nic", 0, 'z');
Inventory::Inventory(){
    for(int i=0; i<m_lektvary.size(); i++){
        m_lektvary.at(i)=zadnyLektvar;
    }
}
void Inventory::printObsah(){
    std::cout << "Tve lektvary:\n";
    printLektvary();
    if(m_specItemy.size() == 0){
        std::cout << "\nZatim nemas zadne specialni predmety. Pokracuj v hrani a hledej!\n";
    }else {
        std::cout << "\nTve specialni predmety:\n";
        printSpecItemy();
    }
}
void Inventory::printLektvary(){
    for(int i=0; i<m_lektvary.size(); i++){
        std::cout << i << ") "; m_lektvary.at(i)->printInfo();
    }
}
void Inventory::printSpecItemy(){
    for (int i = 0; i < m_specItemy.size(); i++) {
        std::cout << i << ") " << m_specItemy.at(i)->getJmeno();
    }
}

void Inventory::pridatLektvar(char n){
    bool truefalse=0;
    int poc =0;
    for(int i=0; i<m_lektvary.size(); i++){
        while(truefalse==0) {
            if (m_lektvary.at(i) == zadnyLektvar) {
                if(n == 'h'){
                    m_lektvary.at(i) = new Lektvar("Healing lektvar", 20, 'h');
                } else if(n == 's'){
                    m_lektvary.at(i) = new Lektvar("Strength lektvar", 20, 's');
                } else if(n == 'd'){
                    m_lektvary.at(i) = new Lektvar("Defense lektvar", 20, 'd');
                }
                truefalse=1;
                poc++;
            }else if(poc==4 && truefalse==0){
                //mocLektvaru(lektvar);
            }
        }
    }
}
void Inventory::odhoditLektvar(int ktery){
    std::cout << "Odhazujes " << m_lektvary.at(ktery)->getJmeno() << "\n";
    m_lektvary.at(ktery) = zadnyLektvar;
    std::cout << std::endl;
}

void Inventory::pridatSpecItem(SpecItem* predmet){
    m_specItemy.push_back(predmet);
}
void Inventory::pouzitSpecItem(int ktery){
    int puvodniVelikost = m_specItemy.size();
    std::cout << "Pouzil jsi " << m_specItemy.at(ktery)->getJmeno() << std::endl;
    m_specItemy.at(ktery) = nullptr;
    int i = ktery;
    while(i < puvodniVelikost-1){
        m_specItemy.at(i) = m_specItemy.at(i+1);
        i++;
    }
    m_specItemy.pop_back();
}

Lektvar* Inventory::getLektvar(int ktery){
    return m_lektvary.at(ktery);
}