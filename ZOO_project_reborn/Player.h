//
// Created by david on 14.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_PLAYER_H
#define ZOO_PROJECT_REBORN_PLAYER_H
#include <vector>
#include "Itemy/Pet.h"
#include "Itemy/Brneni.h"
#include "Itemy/Zbran.h"
#include "Inventory.h"
#include "Monstra/Nepritel.h"
#include "Monstra/TovarnaMonster.h"
#include "Monstra/TovarnaStrednichMonster.h"
#include "Monstra/TovarnaTezkychMonster.h"

class Player {
    std::string m_jmeno;
    double m_zdravi;
    double m_sila;
    double m_obrana;
    double m_zlataky;
    int m_obtiznost;
    TovarnaMonster* m_tovarna;
    Pet* m_pet;
    Brneni* m_brneni;
    Zbran* m_zbran;
    Inventory* m_inventar;
public:
    Player(std::string jmeno);
    std::string getJmeno();
    double getZdravi();
    double getSila();
    double getObrana();
    double getZlataky();
    Pet* getPet();
    Brneni* getBrneni();
    Zbran* getZbran();
    int getObtiznost();
    TovarnaMonster* getTovarna();

    void setJmeno(std::string jmeno);
    void setObtiznost(int obtiznost);
    void pridatZdravi(double kolik);
    void zkontrolujZdravi();
    void pridatSilu(double kolik);
    void pridatObranu(double kolik);
    void pridatZlataky(double kolik);
    void setPet(Pet* pet);
    void setBrneni(Brneni* brneni);
    void setZbran(Zbran* zbran);
    void printInfo();
    void checkInventory();

    void pridatLektvar(char n);
    void vypijLektvar(Lektvar* lektvar);
    char rozpoznaniLektvaru(Lektvar* lektvar);
    void vypisBonusLektvaru(Lektvar* lektvar);

    void bojuj(Nepritel* nepritel);
    void printInfoOSouboji(Nepritel* nepritel, bool &skoncit);
    void etapaBoje(Nepritel* &nepritel, bool &skoncit);
};


#endif //ZOO_PROJECT_REBORN_PLAYER_H
