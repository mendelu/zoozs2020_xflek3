//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_TEZKYKYKLOP_H
#define ZOO_PROJECT_REBORN_TEZKYKYKLOP_H
#include "Kyklop.h"

class TezkyKyklop: public Kyklop {
public:
    TezkyKyklop(double silaUtoku);
};


#endif //ZOO_PROJECT_REBORN_TEZKYKYKLOP_H
