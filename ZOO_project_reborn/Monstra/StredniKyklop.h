//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_STREDNIKYKLOP_H
#define ZOO_PROJECT_REBORN_STREDNIKYKLOP_H
#include "Kyklop.h"

class StredniKyklop: public Kyklop {
public:
    StredniKyklop(double silaUtoku);
};


#endif //ZOO_PROJECT_REBORN_STREDNIKYKLOP_H
