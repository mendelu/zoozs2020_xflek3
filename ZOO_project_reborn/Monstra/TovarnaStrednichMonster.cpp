//
// Created by Jan Mecerod on 24.01.2021.
//

#include "TovarnaStrednichMonster.h"

Nepritel* TovarnaStrednichMonster::getKyklop() {
    return new StredniKyklop(15);
}

Nepritel* TovarnaStrednichMonster::getKentaur(){
    return new StredniKentaur(10);
}
