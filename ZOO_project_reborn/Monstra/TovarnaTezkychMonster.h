//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_TOVARNATEZKYCHMONSTER_H
#define ZOO_PROJECT_REBORN_TOVARNATEZKYCHMONSTER_H
#include "TovarnaMonster.h"
#include "TezkyKyklop.h"
#include "TezkyKentaur.h"

class TovarnaTezkychMonster: public TovarnaMonster{
public:
    Nepritel* getKyklop();
    Nepritel* getKentaur();
};

#endif //ZOO_PROJECT_REBORN_TOVARNATEZKYCHMONSTER_H
