//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_STREDNIKENTAUR_H
#define ZOO_PROJECT_REBORN_STREDNIKENTAUR_H
#include "Kentaur.h"

class StredniKentaur: public Kentaur {
public:
    StredniKentaur(double silaUtoku);
};


#endif //ZOO_PROJECT_REBORN_STREDNIKENTAUR_H
