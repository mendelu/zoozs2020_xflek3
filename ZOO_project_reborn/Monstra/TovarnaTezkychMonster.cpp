//
// Created by Jan Mecerod on 24.01.2021.
//

#include "TovarnaTezkychMonster.h"

Nepritel* TovarnaTezkychMonster::getKyklop() {
    return new TezkyKyklop(20);
}

Nepritel* TovarnaTezkychMonster::getKentaur() {
    return new TezkyKentaur(15);
}