//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_KYKLOP_H
#define ZOO_PROJECT_REBORN_KYKLOP_H
#include "Nepritel.h"

class Kyklop: public Nepritel {
public:
    double getSilaUtoku();
    ~Kyklop();
    Kyklop(std::string jmeno, double silaUtoku, double zivoty);
};


#endif //ZOO_PROJECT_REBORN_KYKLOP_H
