//
// Created by Jan Mecerod on 24.01.2021.
//

#include "Nepritel.h"

Nepritel::Nepritel(std::string jmeno, double silaUtoku, double zivoty){
    m_jmeno = jmeno;
    m_silaUtoku = silaUtoku;
    m_zivoty = zivoty;
}

double Nepritel::getSilaUtoku(){
    return m_silaUtoku;
}

double Nepritel::getZivoty(){
    return m_zivoty;
}

std::string Nepritel::getJmeno(){
    return m_jmeno;
}

void Nepritel::setZivoty(double kolik){
    m_zivoty += kolik;
}