//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_TOVARNAMONSTER_H
#define ZOO_PROJECT_REBORN_TOVARNAMONSTER_H
#include "Kyklop.h"
#include "Kentaur.h"

class TovarnaMonster {
public:
    virtual Nepritel* getKyklop() = 0;
    virtual Nepritel* getKentaur() = 0;
};


#endif //ZOO_PROJECT_REBORN_TOVARNAMONSTER_H
