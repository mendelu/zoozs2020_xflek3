//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_NEPRITEL_H
#define ZOO_PROJECT_REBORN_NEPRITEL_H
#include <iostream>

class Nepritel {
    std::string m_jmeno;
    double m_silaUtoku;
    double m_zivoty;
public:
    double getSilaUtoku();
    double getZivoty();
    std::string getJmeno();
    void setZivoty(double kolik);
    Nepritel(std::string jmeno, double silaUtoku, double zivoty);
    ~Nepritel();
};


#endif //ZOO_PROJECT_REBORN_NEPRITEL_H
