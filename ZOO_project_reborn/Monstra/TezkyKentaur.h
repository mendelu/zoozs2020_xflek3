//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_TEZKYKENTAUR_H
#define ZOO_PROJECT_REBORN_TEZKYKENTAUR_H
#include "Kentaur.h"

class TezkyKentaur: public Kentaur {
public:
    TezkyKentaur(double silaUtoku);
};


#endif //ZOO_PROJECT_REBORN_TEZKYKENTAUR_H
