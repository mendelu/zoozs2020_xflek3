//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_TOVARNASTREDNICHMONSTER_H
#define ZOO_PROJECT_REBORN_TOVARNASTREDNICHMONSTER_H
#include "TovarnaMonster.h"
#include "StredniKyklop.h"
#include "StredniKentaur.h"
#include "Nepritel.h"

class TovarnaStrednichMonster: public TovarnaMonster{
public:
    Nepritel* getKyklop();
    Nepritel* getKentaur();
};


#endif //ZOO_PROJECT_REBORN_TOVARNASTREDNICHMONSTER_H
