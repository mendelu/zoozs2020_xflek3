//
// Created by Jan Mecerod on 24.01.2021.
//

#ifndef ZOO_PROJECT_REBORN_KENTAUR_H
#define ZOO_PROJECT_REBORN_KENTAUR_H
#include "Nepritel.h"

class Kentaur: public Nepritel {
public:
    double getSilaUtoku();
    ~Kentaur();
    Kentaur(std::string jmeno, double silaUtoku, double zivoty);
};


#endif //ZOO_PROJECT_REBORN_KENTAUR_H
